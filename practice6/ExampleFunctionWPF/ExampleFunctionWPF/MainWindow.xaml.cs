﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Globalization;

namespace ExampleFunctionWPF
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DrawingGroup drawingGroup = new DrawingGroup();

        // ручки и кисти для отрисовки графика функции
        Pen backgroundPen;
        Pen axesPen;
        Pen axesLinesPen;
        Pen functionPen;
        Pen legendPen;
        Brush backgroundBrush;
        Brush axesBrush;
        Brush axesLinesBrush;
        Brush legendBrush;

        // настройка ручек и кистей, указанных выше
        Brush backgroundColor = Brushes.Red;
        Brush axesColor = Brushes.Gray;
        Brush axesLinesColor = Brushes.Gray;
        Brush functionColor = Brushes.Blue;
        Brush legendColor = Brushes.Gray;
        float backgroundThick = 0.005f;
        float axesThick = 0.003f;
        float axesLinesThick = 0.01f;
        float functionThick = 0.005f;
        float legendThick = 0.003f;

        // Настройка легенды графика функции
        CultureInfo legendCultureInfo = CultureInfo.InvariantCulture;
        FlowDirection legendFlowDirection = FlowDirection.LeftToRight;
        Typeface legendTypeface = new Typeface("Verdana");
        float legendSize = 0.2f;
        Brush foregroundBrush = Brushes.Black;
        FontWeight legendFontWeight = FontWeights.Bold;
        
        private double[,] valuesFunction; // двумерный массив со значениями x, y точек графика
        private int elementsCount = 0; // кол-во элементов в массиве
        const int dotsCount = 200; // количество точек, хранящихся в массиве valuesFunction
        int minBorderX = -10, maxBorderX = 10; // границы аргумента x
        int minBorderY = -2, maxBorderY = 2; // границы аргумента y

        public MainWindow()
        {
            InitializeComponent();

            // инициализируем ручки для отрисовки графика функции
            backgroundPen = new Pen(Brushes.Red, backgroundThick);
            axesPen = new Pen(axesColor, axesThick);
            axesLinesPen = new Pen(axesLinesColor, axesLinesThick);
            functionPen = new Pen(functionColor, functionThick);
            legendPen = new Pen(legendColor, legendThick);

            // инициализируем ручки для отрисовки графика функции
            backgroundBrush = Brushes.Beige;
            axesBrush = Brushes.Beige;
            axesLinesBrush = Brushes.Beige;
            legendBrush = Brushes.LightGray;

            DataFill(); // заполняем массив, содержащий координаты точек графика функции
            Execute(); // выполняем отрисовки слоёв

            FirstImage.Source = new DrawingImage(drawingGroup); // отображаем график функции на экране
        }

        // функция, заполняющая массив координат графика функции
        private void DataFill()
        {
            float x = 0, y = 0; // временные переменные
            valuesFunction = new double[dotsCount, 2]; // массив из dotsCount точек
            elementsCount = 0; // счётчик элементов массива
            for (x = minBorderX; x < maxBorderX; x += (float)(maxBorderX - minBorderX) / (float)dotsCount) // вычисление y для текущего x
            {
                if (elementsCount >= dotsCount) // если массив valuesFunction заполнен
                    break;
                if (x != 0) // значение функции y = f(x)
                    y = (float)Math.Sin(x) / x;
                else
                    y = 1;
                valuesFunction[elementsCount, 0] = x;
                valuesFunction[elementsCount, 1] = y;
                elementsCount++; // увеличиваем счётчик
            }
        }

        // функция, выполняющая отрисовку каждого слоя
        private void Execute()
        {
            WindowBackground(); // фоновый слой
            Grid(); // слой сетки
            Function(); // слой графика функции
            Legend(); // слой легенды
        }

        // функция, отрисовывающая фоновый слой
        private void WindowBackground()
        {
            //создаём объекты для описания геометрических фигур
            GeometryDrawing geometryDrawing = new GeometryDrawing();
            RectangleGeometry rectangleGeometry = new RectangleGeometry();

            // описываем и сохраняем геометрию прямоугольника
            rectangleGeometry.Rect = new Rect(minBorderX, minBorderY, (maxBorderX - minBorderX), (maxBorderY - minBorderY));
            geometryDrawing.Geometry = rectangleGeometry;
            
            // настраиваем перо и кисть
            geometryDrawing.Pen = backgroundPen;
            geometryDrawing.Brush = backgroundBrush;

            // добавляем готовый слой в контейнер отображения
            drawingGroup.Children.Add(geometryDrawing);
        }

        // функция, отрисовывающая слои сетки
        private void Grid()
        {
            // сетка
            GeometryGroup geometryGroup = new GeometryGroup();

            // создаём и добавляем в коллекцию десять параллельных линий
            for (float i = minBorderX; i <= maxBorderX; i++)
            {
                LineGeometry line = new LineGeometry(new Point(i, minBorderY), new Point(i, maxBorderY));
                geometryGroup.Children.Add(line);
            }

            for (float i = minBorderY; i <= maxBorderY; i++)
            {
                LineGeometry line = new LineGeometry(new Point(minBorderX, i), new Point(maxBorderX, i));
                geometryGroup.Children.Add(line);
            }

            // сохраняем описание геометрии
            GeometryDrawing geometryDrawing = new GeometryDrawing();
            geometryDrawing.Geometry = geometryGroup;

            // настраиваем перо и кисть
            geometryDrawing.Pen = axesPen;
            geometryDrawing.Brush = axesBrush;

            // добавляем готовый слой в контейнер отображения
            drawingGroup.Children.Add(geometryDrawing);

            // Оси графика
            GeometryGroup axesLinesGroup = new GeometryGroup();
            LineGeometry lineOX = new LineGeometry(new Point(minBorderX, 0), new Point(maxBorderX, 0));
            LineGeometry lineOY = new LineGeometry(new Point(0, minBorderY), new Point(0, maxBorderY));
            LineGeometry lineOXArrowTop = new LineGeometry(new Point(maxBorderX, 0), new Point(maxBorderX - 0.2, 0.2));
            LineGeometry lineOXArrowBottom = new LineGeometry(new Point(maxBorderX, 0), new Point(maxBorderX - 0.2, -0.2));
            LineGeometry lineOYArrowLeft = new LineGeometry(new Point(0, minBorderY), new Point(-0.2, minBorderY + 0.2));
            LineGeometry lineOYArrowRight = new LineGeometry(new Point(0, minBorderY), new Point(0.2, minBorderY + 0.2));
            axesLinesGroup.Children.Add(lineOX);
            axesLinesGroup.Children.Add(lineOY);
            axesLinesGroup.Children.Add(lineOXArrowTop);
            axesLinesGroup.Children.Add(lineOXArrowBottom);
            axesLinesGroup.Children.Add(lineOYArrowLeft);
            axesLinesGroup.Children.Add(lineOYArrowRight);

            // сохраняем описание геометрии
            GeometryDrawing axesLinesDrawing = new GeometryDrawing();
            axesLinesDrawing.Geometry = axesLinesGroup;

            // настраиваем перо и кисть
            axesLinesDrawing.Pen = axesLinesPen;
            axesLinesDrawing.Brush = axesLinesBrush;

            // добавляем готовый слой в контейнер отображения
            drawingGroup.Children.Add(axesLinesDrawing);
        }

        // функция, отрисовывающая слой графика функции
        private void Function()
        {
            // график функции
            GeometryGroup geometryGroup = new GeometryGroup();

            // строим описание графика функции при помощи линий
            for (int i = 0; i < elementsCount - 1; i++)
            {
                LineGeometry line = new LineGeometry(new Point(valuesFunction[i, 0], -valuesFunction[i, 1]), new Point(valuesFunction[i + 1, 0], -valuesFunction[i + 1, 1]));
                geometryGroup.Children.Add(line);
            }

            // сохраняем описание геометрии
            GeometryDrawing geometryDrawing = new GeometryDrawing();
            geometryDrawing.Geometry = geometryGroup;
            
            // настраиваем перо
            geometryDrawing.Pen = functionPen;

            // добавляем готовый слой в контейнер отображения
            drawingGroup.Children.Add(geometryDrawing);
        }

        // функция, отрисовывающая слой легенды
        private void Legend()
        {
            // легенда
            GeometryGroup geometryGroup = new GeometryGroup();

            // настраиваем текст рядом с осью OY
            for (int i = 0; i <= (maxBorderY - minBorderY); i++)
            {
                FormattedText formattedText = new FormattedText(String.Format("{0:f0}", maxBorderY - i), legendCultureInfo, legendFlowDirection, legendTypeface, legendSize, foregroundBrush);
                formattedText.SetFontWeight(legendFontWeight);

                Geometry geometry = formattedText.BuildGeometry(new Point(-0.35 + minBorderX, -0.2 - (maxBorderY - i)));
                geometryGroup.Children.Add(geometry);
            }

            // настраиваем текст рядом с осью OX
            for (int i = 0; i <= (maxBorderX - minBorderX); i++)
            {
                FormattedText formattedText = new FormattedText(String.Format("{0:f0}", maxBorderX - i), legendCultureInfo, legendFlowDirection, legendTypeface, legendSize, foregroundBrush);
                formattedText.SetFontWeight(legendFontWeight);

                Geometry geometry = formattedText.BuildGeometry(new Point(-0.2 + maxBorderX - i, 0.1 + maxBorderY));
                geometryGroup.Children.Add(geometry);
            }

            // подпись оси OX
            FormattedText formattedTextOX = new FormattedText("OX", legendCultureInfo, legendFlowDirection, legendTypeface, legendSize, foregroundBrush);
            formattedTextOX.SetFontWeight(legendFontWeight);
            Geometry geometryOX = formattedTextOX.BuildGeometry(new Point(-0.4 + maxBorderX, 0.2));
            geometryGroup.Children.Add(geometryOX);
            
            // подпись оси OY
            FormattedText formattedTextOY = new FormattedText("OY", legendCultureInfo, legendFlowDirection, legendTypeface, legendSize, foregroundBrush);
            formattedTextOY.SetFontWeight(legendFontWeight);
            Geometry geometryOY = formattedTextOY.BuildGeometry(new Point(0.25, minBorderY));
            geometryGroup.Children.Add(geometryOY);
            
            // подпись центра пересечения осей
            FormattedText formattedTextZero = new FormattedText("O", legendCultureInfo, legendFlowDirection, legendTypeface, legendSize, foregroundBrush);
            formattedTextZero.SetFontWeight(legendFontWeight);
            Geometry geometryZero = formattedTextZero.BuildGeometry(new Point(0.05, 0.05));
            geometryGroup.Children.Add(geometryZero);

            // сохраняем описание геометрии
            GeometryDrawing geometryDrawing = new GeometryDrawing();
            geometryDrawing.Geometry = geometryGroup;

            // настраиваем перо и кисть
            geometryDrawing.Pen = legendPen;
            geometryDrawing.Brush = legendBrush;

            // добавляем готовый слой в контейнер отображения
            drawingGroup.Children.Add(geometryDrawing);
        }
    }
}
