﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Example3DFigureWPF
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        const int decagonalFaceCount = 12; // количество десятиугольных граней
        const int triangularFaceCount = 20; // количество треугольных граней
        private GeometryModel3D[] meshGeometry; // геометрическая модель, которую будем рисовать
        
        private bool mouseDown; // переменная-флаг, отвечающая за нажатие клавиши
        private Point mouseLastPosition; // координаты последнего местоположения курсора мыши

        public MainWindow()
        {
            InitializeComponent(); 

            // инициализация сетки, по которой будет строиться фигура
            meshGeometry = new GeometryModel3D[triangularFaceCount + 8  * decagonalFaceCount];

            BuildSolid(); // вызываем функцию построения фигуры
        }

        // функция построения сетки из треугольников
        private void drawTriangle(Point3D point1, Point3D point2, Point3D point3, int index, Brush brush)
        {
            MeshGeometry3D mesh = new MeshGeometry3D(); // сетка, на основе которой будет строиться очередная грань фигуры

            // задаём положение треугольника в пространстве
            mesh.Positions.Add(point1);
            mesh.Positions.Add(point2);
            mesh.Positions.Add(point3);

            // строим треугольник по сетке
            mesh.TriangleIndices.Add(0);
            mesh.TriangleIndices.Add(1);
            mesh.TriangleIndices.Add(2);

            meshGeometry[index] = new GeometryModel3D(mesh, new DiffuseMaterial(brush)); // создаём грань из сетки
            meshGeometry[index].Transform = new Transform3DGroup(); // создаём трансформацию для модели
            group.Children.Add(meshGeometry[index]); // группируем грани модели
        }

        // функция, отрисовывающая усечённый додекаэдр
        private void BuildSolid()
        {
            const int verticesCount = 60 + 1; // количество вершин в фигуре
            float goldenRatio = (1 + (float)Math.Sqrt(5)) / 2; // значение золотого сечения
            float[][] vertices; // массив координат вершин фигуры
            vertices = new float[verticesCount][]
            {
                new float[] {0, 0, 0}, // фиктивная вершина (для упрощения работы с гранями)

                // первый набор вершин

                new float[] {0, 1 / goldenRatio, (2 + goldenRatio)},
                new float[] {0, 1 / goldenRatio, -(2 + goldenRatio)},
                new float[] {0, -1 / goldenRatio, (2 + goldenRatio)},
                new float[] {0, -1 / goldenRatio, -(2 + goldenRatio)},

                new float[] {(2 + goldenRatio), 0, 1 / goldenRatio},
                new float[] {(2 + goldenRatio), 0, -1 / goldenRatio},
                new float[] {-(2 + goldenRatio), 0, 1 / goldenRatio},
                new float[] {-(2 + goldenRatio), 0, -1 / goldenRatio},

                new float[] {1 / goldenRatio, (2 + goldenRatio), 0},
                new float[] {1 / goldenRatio, -(2 + goldenRatio), 0},
                new float[] {-1 / goldenRatio, (2 + goldenRatio), 0},
                new float[] {-1 / goldenRatio, -(2 + goldenRatio), 0},

                // второй набор вершин

                new float[] {1 / goldenRatio, goldenRatio, 2 * goldenRatio},
                new float[] {1 / goldenRatio, goldenRatio, -2 * goldenRatio},
                new float[] {1 / goldenRatio, -goldenRatio, 2 * goldenRatio},
                new float[] {1 / goldenRatio, -goldenRatio, -2 * goldenRatio},
                new float[] {-1 / goldenRatio, goldenRatio, 2 * goldenRatio},
                new float[] {-1 / goldenRatio, goldenRatio, -2 * goldenRatio},
                new float[] {-1 / goldenRatio, -goldenRatio, 2 * goldenRatio},
                new float[] {-1 / goldenRatio, -goldenRatio, -2 * goldenRatio},

                new float[] {2 * goldenRatio, 1 / goldenRatio, goldenRatio},
                new float[] {2 * goldenRatio, 1 / goldenRatio, -goldenRatio},
                new float[] {2 * goldenRatio, -1 / goldenRatio, goldenRatio},
                new float[] {2 * goldenRatio, -1 / goldenRatio, -goldenRatio},
                new float[] {-2 * goldenRatio, 1 / goldenRatio, goldenRatio},
                new float[] {-2 * goldenRatio, 1 / goldenRatio, -goldenRatio},
                new float[] {-2 * goldenRatio, -1 / goldenRatio, goldenRatio},
                new float[] {-2 * goldenRatio, -1 / goldenRatio, -goldenRatio},

                new float[] {goldenRatio, 2 * goldenRatio, 1 / goldenRatio},
                new float[] {goldenRatio, 2 * goldenRatio, -1 / goldenRatio},
                new float[] {goldenRatio, -2 * goldenRatio, 1 / goldenRatio},
                new float[] {goldenRatio, -2 * goldenRatio, -1 / goldenRatio},
                new float[] {-goldenRatio, 2 * goldenRatio, 1 / goldenRatio},
                new float[] {-goldenRatio, 2 * goldenRatio, -1 / goldenRatio},
                new float[] {-goldenRatio, -2 * goldenRatio, 1 / goldenRatio},
                new float[] {-goldenRatio, -2 * goldenRatio, -1 / goldenRatio},

                // третий набор вершин

                new float[] {goldenRatio, 2, (float)Math.Pow(goldenRatio, 2)},
                new float[] {goldenRatio, 2, -(float)Math.Pow(goldenRatio, 2)},
                new float[] {goldenRatio, -2, (float)Math.Pow(goldenRatio, 2)},
                new float[] {goldenRatio, -2, -(float)Math.Pow(goldenRatio, 2)},
                new float[] {-goldenRatio, 2, (float)Math.Pow(goldenRatio, 2)},
                new float[] {-goldenRatio, 2, -(float)Math.Pow(goldenRatio, 2)},
                new float[] {-goldenRatio, -2, (float)Math.Pow(goldenRatio, 2)},
                new float[] {-goldenRatio, -2, -(float)Math.Pow(goldenRatio, 2)},

                new float[] {(float)Math.Pow(goldenRatio, 2), goldenRatio, 2},
                new float[] {(float)Math.Pow(goldenRatio, 2), goldenRatio, -2},
                new float[] {(float)Math.Pow(goldenRatio, 2), -goldenRatio, 2},
                new float[] {(float)Math.Pow(goldenRatio, 2), -goldenRatio, -2},
                new float[] {-(float)Math.Pow(goldenRatio, 2), goldenRatio, 2},
                new float[] {-(float)Math.Pow(goldenRatio, 2), goldenRatio, -2},
                new float[] {-(float)Math.Pow(goldenRatio, 2), -goldenRatio, 2},
                new float[] {-(float)Math.Pow(goldenRatio, 2), -goldenRatio, -2},

                new float[] {2, (float)Math.Pow(goldenRatio, 2), goldenRatio},
                new float[] {2, (float)Math.Pow(goldenRatio, 2), -goldenRatio},
                new float[] {2, -(float)Math.Pow(goldenRatio, 2), goldenRatio},
                new float[] {2, -(float)Math.Pow(goldenRatio, 2), -goldenRatio},
                new float[] {-2, (float)Math.Pow(goldenRatio, 2), goldenRatio},
                new float[] {-2, (float)Math.Pow(goldenRatio, 2), -goldenRatio},
                new float[] {-2, -(float)Math.Pow(goldenRatio, 2), goldenRatio},
                new float[] {-2, -(float)Math.Pow(goldenRatio, 2), -goldenRatio}
            };

            int[,] triangularFaces; // массив вершин для построения треугольных граней
            triangularFaces = new int[,]
            {
                // в плоскости XoY
                {1, 13, 17},
                {18, 14, 2},
                {15, 3, 19},
                {20, 4, 16},
                {45, 53, 37},
                {38, 54, 46},
                {55, 47, 39},
                {40, 48, 56},    
                {57, 49, 41},
                {42, 50, 58},
                {59, 43, 51},
                {52, 44, 60},
               
                // в плоскости XoZ
                {29, 30, 9},
                {10, 32, 31},
                {11, 34, 33},
                {35, 36, 12},
                
                // в плоскости YoZ
                {22, 6, 24},
                {28, 8, 26},
                {5, 21, 23},
                {27, 25, 7}
            };

            int[,] decagonalFaces; // массив вершин для построения десятиугольных граней
            decagonalFaces = new int[,]
            {
                {1, 17, 41, 49, 25, 27, 51, 43, 19, 3},
                {4, 20, 44, 52, 28, 26, 50, 42, 18, 2},
                {3, 15, 39, 47, 23, 21, 45, 37, 13, 1},
                {2, 14, 38, 46, 22, 24, 48, 40, 16, 4},
                {29, 9, 11, 33, 57, 41, 17, 13, 37, 53},
                {54, 38, 14, 18, 42, 58, 34, 11, 9, 30},
                {15, 19, 43, 59, 35, 12, 10, 31, 55, 39},
                {40, 56, 32, 10, 12, 36, 60, 44, 20, 16},
                {53, 45, 21, 5, 6, 22, 46, 54, 30, 29},
                {6, 5, 23, 47, 55, 31, 32, 56, 48, 24},
                {49, 57, 33, 34, 58, 50, 26, 8, 7, 25},
                {7, 8, 28, 52, 60, 36, 35, 59, 51, 27}
            };

            SolidColorBrush[] brushes; // массив кистей для окрашивания граней фигуры
            brushes = new SolidColorBrush[triangularFaceCount + decagonalFaceCount]
            {
                new SolidColorBrush(Color.FromArgb(255, 255, 0, 0)),
                new SolidColorBrush(Color.FromArgb(255, 255, 211, 0)),
                new SolidColorBrush(Color.FromArgb(255, 191, 167, 48)),
                new SolidColorBrush(Color.FromArgb(255, 166, 137, 0)),
                new SolidColorBrush(Color.FromArgb(255, 255, 64, 64)),
                new SolidColorBrush(Color.FromArgb(255, 255, 116, 0)),
                new SolidColorBrush(Color.FromArgb(255, 0, 204, 0)),
                new SolidColorBrush(Color.FromArgb(255, 29, 115, 115)),
                new SolidColorBrush(Color.FromArgb(255, 0, 99, 99)),
                new SolidColorBrush(Color.FromArgb(255, 51, 204, 204)),
                new SolidColorBrush(Color.FromArgb(255, 103, 230, 103)),
                new SolidColorBrush(Color.FromArgb(255, 255, 231, 115)),
                new SolidColorBrush(Color.FromArgb(255, 0, 133, 0)),
                new SolidColorBrush(Color.FromArgb(255, 57, 230, 57)),
                new SolidColorBrush(Color.FromArgb(255, 92, 204, 204)),
                new SolidColorBrush(Color.FromArgb(255, 191, 113, 48)),
                new SolidColorBrush(Color.FromArgb(255, 57, 20, 175)),
                new SolidColorBrush(Color.FromArgb(255, 65, 44, 132)),
                new SolidColorBrush(Color.FromArgb(255, 32, 7, 114)),
                new SolidColorBrush(Color.FromArgb(255, 0, 153, 153)),
                new SolidColorBrush(Color.FromArgb(255, 38, 153, 38)),
                new SolidColorBrush(Color.FromArgb(255, 255, 222, 64)),
                new SolidColorBrush(Color.FromArgb(255, 255, 150, 64)),
                new SolidColorBrush(Color.FromArgb(255, 106, 72, 215)),
                new SolidColorBrush(Color.FromArgb(255, 135, 110, 215)),
                new SolidColorBrush(Color.FromArgb(255, 166, 75, 0)),
                new SolidColorBrush(Color.FromArgb(255, 255, 178, 115)),
                new SolidColorBrush(Color.FromArgb(255, 255, 115, 115)),
                new SolidColorBrush(Color.FromArgb(255, 191, 48, 48)),
                new SolidColorBrush(Color.FromArgb(255, 72, 3, 111)),
                new SolidColorBrush(Color.FromArgb(255, 166, 166, 0)),
                new SolidColorBrush(Color.FromArgb(255, 166, 0, 0))
            };


            Point3D temp1, temp2, temp3; // временные переменные для хранения координат вершин
            int tempIndex = 0; // текущая сетка, с которой мы работаем
            for (int face = 0; face < triangularFaceCount; face++) // построение треугольных граней
            {
                temp1 = new Point3D(vertices[triangularFaces[face, 0]][0], vertices[triangularFaces[face, 0]][1], vertices[triangularFaces[face, 0]][2]);
                temp2 = new Point3D(vertices[triangularFaces[face, 1]][0], vertices[triangularFaces[face, 1]][1], vertices[triangularFaces[face, 1]][2]);
                temp3 = new Point3D(vertices[triangularFaces[face, 2]][0], vertices[triangularFaces[face, 2]][1], vertices[triangularFaces[face, 2]][2]);

                drawTriangle(temp1, temp2, temp3, face, brushes[face]);
            }
            for (int face = 0; face < decagonalFaceCount; face++) // построение десятиугольных граней
                for (int i = 1; i < 9; i++)
                {
                    temp1 = new Point3D(vertices[decagonalFaces[face, 0]][0], vertices[decagonalFaces[face, 0]][1], vertices[decagonalFaces[face, 0]][2]);
                    temp2 = new Point3D(vertices[decagonalFaces[face, i]][0], vertices[decagonalFaces[face, i]][1], vertices[decagonalFaces[face, i]][2]);
                    temp3 = new Point3D(vertices[decagonalFaces[face, i + 1]][0], vertices[decagonalFaces[face, i + 1]][1], vertices[decagonalFaces[face, i + 1]][2]);

                    drawTriangle(temp1, temp2, temp3, triangularFaceCount + tempIndex, brushes[triangularFaceCount + face]);
                    tempIndex++;
                }
        }

        // обработчик событий, отвечающий за работу колесиком мыши
        private void Grid_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            camera.Position = new Point3D(camera.Position.X, camera.Position.Y, camera.Position.Z - e.Delta / 500D); // установка позиции камеры
        }

        // обработчик событий, отвечающий за перемещение мыши в окне приложения
        private void Grid_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseDown) // если клавиша мыши нажата
            {
                // вычисляем текущие координаты мыши в координатах viewport, а также перемещение по оси OX и OY
                Point position = Mouse.GetPosition(viewport); 
                Point actualPosition = new Point(position.X - viewport.ActualWidth / 2, viewport.ActualHeight / 2 - position.Y);
                double dx = actualPosition.X - mouseLastPosition.X;
                double dy = actualPosition.Y - mouseLastPosition.Y;

                // осуществляем поворот фигуры в пространстве в зависимости от перемещения мыши по экрану
                double mouseAngle = 0;
                if ((dx != 0) && (dy != 0))
                {
                    mouseAngle = Math.Asin(Math.Abs(dy) / Math.Sqrt(Math.Pow(dx, 2) + Math.Pow(dy, 2)));
                    if ((dx < 0) && (dy > 0))
                        mouseAngle += Math.PI / 2;
                    else if ((dx < 0) && (dy < 0))
                        mouseAngle += Math.PI;
                    else if ((dx > 0) && (dy < 0))
                        mouseAngle += Math.PI * 1.5;
                }
                else if ((dx == 0) && (dy != 0))
                    mouseAngle = Math.Sign(dy) > 0 ? Math.PI / 2 : Math.PI * 1.5;
                else if ((dx != 0) && (dy == 0))
                    mouseAngle = Math.Sign(dx) > 0 ? 0 : Math.PI;

                double axisAngle = mouseAngle + Math.PI / 2;

                Vector3D axis = new Vector3D(Math.Cos(axisAngle) * 4, Math.Sin(axisAngle) * 4, 0);

                double rotation = 0.01 * Math.Sqrt(Math.Pow(dx, 2) + Math.Pow(dy, 2));

                // осуществляем вращение каждой грани фигуры
                for (int i = 0; i < triangularFaceCount + 8 * decagonalFaceCount; i++)
                {
                    Transform3DGroup group = meshGeometry[i].Transform as Transform3DGroup;
                    QuaternionRotation3D rotate = new QuaternionRotation3D(new Quaternion(axis, rotation * 180 / Math.PI));
                    group.Children.Add(new RotateTransform3D(rotate));
                }
                
                mouseLastPosition = actualPosition; // запоминаем последнее положение курсора мыши
            }
        }

        // обработчик событий, отвечающий за нажатие кнопки мыши в окне приложения
        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton != MouseButtonState.Pressed) // если клавиша не была нажата
                return; // ничего не делать
            mouseDown = true; // запомнить, что кнопка мыши была нажата

            // запомнить последние коорди
            Point position = Mouse.GetPosition(viewport);
            mouseLastPosition = new Point(position.X - viewport.ActualWidth / 2, viewport.ActualHeight / 2 - position.Y);
        }

        // обработчик событий, отвечающий за отжатие кнопки мыши в окне приложения
        private void Grid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            mouseDown = false; // запомнить, что кнопка мыши отжата
        }
    }
}
