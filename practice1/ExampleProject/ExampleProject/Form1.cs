﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExampleProject
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Thread stream = new Thread(myFunction);
            stream.Start();

            /* The second example */

            /*Graphics place = this.CreateGraphics();
            Pen redPen = new Pen(Color.Red, 6);
            Pen whitePen = new Pen(Color.White, 6);
            int step = 0;

            for (int i = 0; i < 20; i++)
            {
                step += 10;
                place.DrawEllipse(redPen, 50 + step, 100, 100, 100);
                Thread.Sleep(100);
                place.DrawEllipse(whitePen, 50 + step, 100, 100, 100);
            }*/

            /* The first example */

            /*Graphics place = this.CreateGraphics();
            Pen myPen = new Pen(Color.Red, 6);
            place.DrawRectangle(myPen, 100, 100, 50, 100);

            Point[] polygon =
            {
                new Point(200, 250),
                new Point(250, 220),
                new Point(270, 270),
                new Point(200, 300),
                new Point(200, 250)
            };

            Pen greenPen = new Pen(Color.Green, 3);
            Pen bluePen = new Pen(Color.Blue, 5);
            place.DrawEllipse(greenPen, 200, 100, 200, 100);
            place.DrawPolygon(bluePen, polygon);*/
        }

        public void myFunction()
        {
            Graphics place = this.CreateGraphics();
            Pen redPen = new Pen(Color.Red, 6);
            Pen whitePen = new Pen(Color.White, 6);
            int step = 0;

            for (int i = 0; i < 20; i++)
            {
                step += 10;
                place.DrawEllipse(redPen, 50 + step, 100, 100, 100);
                Thread.Sleep(100);
                place.DrawEllipse(whitePen, 50 + step, 100, 100, 100);
            }
        }
    }
}
