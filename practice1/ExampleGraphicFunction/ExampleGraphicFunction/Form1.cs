﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExampleGraphicFunction
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            myFunction();
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            myFunction();
        }

        public void myFunction()
        {
            Graphics place = this.CreateGraphics();
            place.Clear(Color.White);
            Pen bluePen = new Pen(Color.Blue, 1);
            Pen greenPen = new Pen(Color.Green, 2);
            Font drawFont = new Font("Times New Roman", 12);
            Font signatureFont = new Font("Times New Roman", 7);

            SolidBrush drawBrush = new SolidBrush(Color.Blue);
            StringFormat drawFormat = new StringFormat();
            drawFormat.FormatFlags = StringFormatFlags.DirectionRightToLeft;

            int sizeWidth = Form1.ActiveForm.Width;
            int sizeHeight = Form1.ActiveForm.Height;
            Point center = new Point((int)((sizeWidth / 2) - 8), (int)((sizeHeight / 2) - 19));

            place.DrawLine(bluePen, 10, center.Y, center.X, center.Y);
            place.DrawLine(bluePen, center.X, center.Y, 2 * center.X - 10, center.Y);
            place.DrawLine(bluePen, center.X, 10, center.X, center.Y);
            place.DrawLine(bluePen, center.X, center.Y, center.X, 2 * center.Y - 10);

            place.DrawString("X", drawFont, drawBrush, new PointF(2 * center.X - 5, center.Y + 10), drawFormat);
            place.DrawString("Y", drawFont, drawBrush, new PointF(center.X + 30, 5), drawFormat);
            place.DrawString("0", drawFont, drawBrush, new PointF(center.X, center.Y + 10), drawFormat);

            place.DrawLine(bluePen, center.X, 10, center.X + 5, 20);
            place.DrawLine(bluePen, center.X, 10, center.X - 5, 20);
            place.DrawLine(bluePen, 2 * center.X - 10, center.Y, 2 * center.X - 20, center.Y - 5);
            place.DrawLine(bluePen, 2 * center.X - 10, center.Y, 2 * center.X - 20, center.Y + 5);

            int stepForAxes = 25;
            int lengthBar = 3;
            int maxValueForAxesX = 4;
            int maxValueForAxesY = 9;

            float firstDegreeX = (float)maxValueForAxesX / ((float)(center.X) / (float)stepForAxes); 
            float firstDegreeY = (float)maxValueForAxesY / ((float)(center.Y) / (float)stepForAxes);

            for (int i = center.X, j = center.X, k = 1; i < 2 * center.X - 30; j -= stepForAxes, i += stepForAxes, k++)
            {
                place.DrawLine(bluePen, i, center.Y - lengthBar, i, center.Y + lengthBar);
                place.DrawLine(bluePen, j, center.Y - lengthBar, j, center.Y + lengthBar);

                if (i < 2 * center.X - 55)
                {
                    place.DrawString((k * firstDegreeX).ToString("0.0"), signatureFont, drawBrush, new PointF(i + stepForAxes + 9, center.Y + 6), drawFormat);
                    place.DrawString(((k * firstDegreeX).ToString("0.0").ToString() + "-"), signatureFont, drawBrush, new PointF(j - stepForAxes + 9, center.Y + 6), drawFormat);
                }
            }
            for (int i = center.Y, j = center.Y, k = 1; i < 2 * center.Y - 30; j -= stepForAxes, i += stepForAxes, k++)
            {
                place.DrawLine(bluePen, center.X - lengthBar, i, center.X + lengthBar, i);
                place.DrawLine(bluePen, center.X - lengthBar, j, center.X + lengthBar, j);

                if (i < 2 * center.Y - 55)
                {
                    place.DrawString((k * firstDegreeY).ToString("0.0").ToString(), signatureFont, drawBrush, new PointF(center.X + 25, j - stepForAxes - 6), drawFormat);
                    place.DrawString((k * firstDegreeY).ToString("0.0") + "-", signatureFont, drawBrush, new PointF(center.X + 25, i + stepForAxes - 6), drawFormat);
                }
            }

            int numPoint = 100;

            float[] coordsX = new float[numPoint];
            for(int i = 0; i < numPoint; i++)
            {
                coordsX[i] = (float)maxValueForAxesX / (float)numPoint * (i + 1) - (float)(maxValueForAxesX / 2);
            }

            float[] coordsY = new float[numPoint];
            for (int i = 0; i < numPoint; i++)
            {
                coordsY[i] = (float)Math.Pow(coordsX[i], 3);
            }

            Point[] pointArray = new Point[numPoint];
            float tempX = 1 / firstDegreeX * stepForAxes;
            float tempY = 1 / firstDegreeY * stepForAxes;
            for (int i = 0; i < numPoint; i++)
            {
                pointArray[i].X = center.X + (int)(coordsX[i] * tempX);
                pointArray[i].Y = center.Y - (int)(coordsY[i] * tempY);
            }

            place.DrawLines(greenPen, pointArray);
            place.DrawCurve(greenPen, pointArray);
        }
    }
}
