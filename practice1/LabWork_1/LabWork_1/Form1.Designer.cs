﻿namespace LabWork_1
{
    partial class MainForm
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.ButtonBox = new System.Windows.Forms.GroupBox();
            this.ButtonPanel = new System.Windows.Forms.TableLayoutPanel();
            this.FIOButton = new System.Windows.Forms.Button();
            this.RocketButton = new System.Windows.Forms.Button();
            this.AnimationButton = new System.Windows.Forms.Button();
            this.FunctionButton = new System.Windows.Forms.Button();
            this.SaveButton = new System.Windows.Forms.Button();
            this.LoadButton = new System.Windows.Forms.Button();
            this.pictureBoxView = new System.Windows.Forms.PictureBox();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.ButtonBox.SuspendLayout();
            this.ButtonPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // ButtonBox
            // 
            this.ButtonBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonBox.Controls.Add(this.ButtonPanel);
            this.ButtonBox.Location = new System.Drawing.Point(592, 12);
            this.ButtonBox.Name = "ButtonBox";
            this.ButtonBox.Size = new System.Drawing.Size(140, 201);
            this.ButtonBox.TabIndex = 0;
            this.ButtonBox.TabStop = false;
            this.ButtonBox.Text = "Меню";
            // 
            // ButtonPanel
            // 
            this.ButtonPanel.ColumnCount = 1;
            this.ButtonPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.ButtonPanel.Controls.Add(this.FIOButton, 0, 0);
            this.ButtonPanel.Controls.Add(this.RocketButton, 0, 1);
            this.ButtonPanel.Controls.Add(this.AnimationButton, 0, 2);
            this.ButtonPanel.Controls.Add(this.FunctionButton, 0, 3);
            this.ButtonPanel.Controls.Add(this.SaveButton, 0, 4);
            this.ButtonPanel.Controls.Add(this.LoadButton, 0, 5);
            this.ButtonPanel.Location = new System.Drawing.Point(6, 19);
            this.ButtonPanel.Name = "ButtonPanel";
            this.ButtonPanel.RowCount = 6;
            this.ButtonPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.ButtonPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.ButtonPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.ButtonPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.ButtonPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.ButtonPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.ButtonPanel.Size = new System.Drawing.Size(127, 175);
            this.ButtonPanel.TabIndex = 0;
            // 
            // FIOButton
            // 
            this.FIOButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FIOButton.Location = new System.Drawing.Point(3, 3);
            this.FIOButton.Name = "FIOButton";
            this.FIOButton.Size = new System.Drawing.Size(121, 23);
            this.FIOButton.TabIndex = 0;
            this.FIOButton.Text = "ФИО";
            this.FIOButton.UseVisualStyleBackColor = true;
            this.FIOButton.Click += new System.EventHandler(this.FIOButton_Click);
            // 
            // RocketButton
            // 
            this.RocketButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RocketButton.Location = new System.Drawing.Point(3, 32);
            this.RocketButton.Name = "RocketButton";
            this.RocketButton.Size = new System.Drawing.Size(121, 23);
            this.RocketButton.TabIndex = 1;
            this.RocketButton.Text = "Ракета!";
            this.RocketButton.UseVisualStyleBackColor = true;
            this.RocketButton.Click += new System.EventHandler(this.RocketButton_Click);
            // 
            // AnimationButton
            // 
            this.AnimationButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AnimationButton.Location = new System.Drawing.Point(3, 61);
            this.AnimationButton.Name = "AnimationButton";
            this.AnimationButton.Size = new System.Drawing.Size(121, 23);
            this.AnimationButton.TabIndex = 2;
            this.AnimationButton.Text = "Ракета, лети!";
            this.AnimationButton.UseVisualStyleBackColor = true;
            this.AnimationButton.Click += new System.EventHandler(this.AnimationButton_Click);
            // 
            // FunctionButton
            // 
            this.FunctionButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FunctionButton.Location = new System.Drawing.Point(3, 90);
            this.FunctionButton.Name = "FunctionButton";
            this.FunctionButton.Size = new System.Drawing.Size(121, 23);
            this.FunctionButton.TabIndex = 3;
            this.FunctionButton.Text = "График функции";
            this.FunctionButton.UseVisualStyleBackColor = true;
            this.FunctionButton.Click += new System.EventHandler(this.FunctionButton_Click);
            // 
            // SaveButton
            // 
            this.SaveButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SaveButton.Location = new System.Drawing.Point(3, 119);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(121, 23);
            this.SaveButton.TabIndex = 4;
            this.SaveButton.Text = "Сохранить в файл";
            this.SaveButton.UseVisualStyleBackColor = true;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // LoadButton
            // 
            this.LoadButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LoadButton.Location = new System.Drawing.Point(3, 148);
            this.LoadButton.Name = "LoadButton";
            this.LoadButton.Size = new System.Drawing.Size(121, 24);
            this.LoadButton.TabIndex = 5;
            this.LoadButton.Text = "Открыть файл";
            this.LoadButton.UseVisualStyleBackColor = true;
            this.LoadButton.Click += new System.EventHandler(this.LoadButton_Click);
            // 
            // pictureBoxView
            // 
            this.pictureBoxView.Location = new System.Drawing.Point(12, 12);
            this.pictureBoxView.Name = "pictureBoxView";
            this.pictureBoxView.Size = new System.Drawing.Size(288, 194);
            this.pictureBoxView.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxView.TabIndex = 1;
            this.pictureBoxView.TabStop = false;
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.Filter = "JPG (*.jpg)|*.jpg|BMP (*.bmp)|*.bmp|PNG (*.png)|*.png";
            // 
            // openFileDialog
            // 
            this.openFileDialog.Filter = "JPG (*.jpg)|*.jpg|BMP (*.bmp)|*.bmp|PNG (*.png)|*.png";
            // 
            // pictureBox
            // 
            this.pictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox.Location = new System.Drawing.Point(0, 0);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(744, 516);
            this.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox.TabIndex = 2;
            this.pictureBox.TabStop = false;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(744, 516);
            this.Controls.Add(this.pictureBoxView);
            this.Controls.Add(this.ButtonBox);
            this.Controls.Add(this.pictureBox);
            this.MaximumSize = new System.Drawing.Size(850, 850);
            this.MinimumSize = new System.Drawing.Size(550, 550);
            this.Name = "MainForm";
            this.Text = "Лабораторая работа №1";
            this.ButtonBox.ResumeLayout(false);
            this.ButtonPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox ButtonBox;
        private System.Windows.Forms.TableLayoutPanel ButtonPanel;
        private System.Windows.Forms.Button RocketButton;
        private System.Windows.Forms.Button AnimationButton;
        private System.Windows.Forms.Button FunctionButton;
        private System.Windows.Forms.Button SaveButton;
        private System.Windows.Forms.Button LoadButton;
        private System.Windows.Forms.Button FIOButton;
        private System.Windows.Forms.PictureBox pictureBoxView;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.PictureBox pictureBox;
    }
}

