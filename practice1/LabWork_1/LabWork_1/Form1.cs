﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace LabWork_1
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void FIOButton_Click(object sender, EventArgs e)
        {
            pictureBox.Hide(); // скрываем элемент pictureBox
            pictureBoxView.Show(); // показываем элемент pictureBoxView (для вывода текста в окно приложения)
            this.Resize -= new System.EventHandler(this.MainForm_Resize); // отключаем обработчик события отрисовки окна (для перерисовки графика функции)

            // создаём объект place, куда будем отображать текст и очищаем его
            Graphics place = pictureBoxView.CreateGraphics();
            place.Clear(pictureBoxView.BackColor);

            // настраиваем текст, его размер, начертание, цвет, положение на экране
            string fio = "Новиков Александр";
            string font = "Times New Roman";
            int size = 12;
            FontStyle style = FontStyle.Regular;
            Color color = Color.Black;
            float x = 5;
            float y = 5;

            // создаём шрифт и цвет текста по заданным настройкам выше
            Font fontFIO = new Font(font, size, style);
            SolidBrush brush = new SolidBrush(color);

            // отрисовываем текст
            place.DrawString(fio, fontFIO, brush, x, y);
        }

        private void MainForm_Resize(object sender, EventArgs e)
        {
            DrawFunction(); // в обработчике событий перерисовываем график функции при изменении размеров окна
        }

        private void DrawFunction()
        {
            Bitmap bmp = new Bitmap(pictureBox.Width, pictureBox.Height); // создаём объект bmp, на котором будет отрисовывать график функции
            Graphics place = Graphics.FromImage(bmp); // создаём объект place, который будет выводить изображение из объекта bmp на экран
            place.Clear(pictureBox.BackColor); // очищаем экран
            
            // задаём цвет линий и толщину
            Color axesColor = Color.Blue;
            Color functionLinesColor = Color.White;
            Color functionCurvesColor = Color.Red;
            int axesThick = 1;
            int functionLinesThick = 8;
            int functionCurvesThick = 1;

            // создаём ручки для отрисовки графика функции
            Pen axesPen = new Pen(axesColor, axesThick);
            Pen functionLinesPen = new Pen(functionLinesColor, functionLinesThick);
            Pen functionCurvesPen = new Pen(functionCurvesColor, functionCurvesThick);

            // настраиваем шрифт, цвет текста и его размер
            string font = "Times New Roman";
            Color signatureColor = Color.Blue;
            int signatureSize = 12;
            int numberSize = 7;

            // создаём шрифт из настроек, указанных выше
            Font numberFont = new Font(font, numberSize);
            Font signatureFont = new Font(font, signatureSize);
            SolidBrush signatureBrush = new SolidBrush(signatureColor);

            StringFormat signatureFormat = new StringFormat();
            signatureFormat.FormatFlags = StringFormatFlags.DirectionRightToLeft;

            int formWidth = MainForm.ActiveForm.Width; // длина окна приложения
            int formHeight = MainForm.ActiveForm.Height; // ширина окна приложения
            int paddingOX = 8; // отступ оси OX сверху
            int paddingOY = 19; // отступ оси OY снизу
            int marginAxes = 10; // отступ осей от границы формы

            /* Отрисовка осей OX и OY */
            Point center = new Point((int)((formWidth / 2) - paddingOX), (int)((formHeight / 2) - paddingOY));
            place.DrawLine(axesPen, marginAxes, center.Y, 2 * center.X - marginAxes, center.Y);
            place.DrawLine(axesPen, center.X, marginAxes, center.X, 2 * center.Y - marginAxes);

            int paddingSignX_OX = -5; // отступ знака X относительно оси OX
            int paddingSignX_OY = 10;  // относительно оси OY
            int paddingSignY_OX = 30;  // отступ знака Y относительно оси OX
            int paddingSignY_OY = 5;   // оотносительно оси OY
            int paddingSign0_OX = 0;   // отступ нуля относительно оси OX
            int paddingSign0_OY = 10;  // относительно оси OY

            /* Отрисовка знаков X, Y и нуля */
            place.DrawString("X", signatureFont, signatureBrush, new PointF(2 * center.X + paddingSignX_OX, center.Y + paddingSignX_OY), signatureFormat);
            place.DrawString("Y", signatureFont, signatureBrush, new PointF(center.X + paddingSignY_OX, paddingSignY_OY), signatureFormat);
            place.DrawString("0", signatureFont, signatureBrush, new PointF(center.X + paddingSign0_OX, center.Y + paddingSign0_OY), signatureFormat);

            int paddingArrowX_OX = 5;  // отступ стрелочки для OX относительно оси OX
            int paddingArrowX_OY = 20; // относительно оси OY
            int paddingArrowY_OX = 20; // отступ стрелочки для OY относительно оси OX
            int paddingArrowY_OY = 5;  // относительно оси OY

            /* Отрисовка стрелочек для осей */
            place.DrawLine(axesPen, center.X, marginAxes, center.X + paddingArrowX_OX, paddingArrowX_OY); // OX
            place.DrawLine(axesPen, center.X, marginAxes, center.X - paddingArrowX_OX, paddingArrowX_OY);
            place.DrawLine(axesPen, 2 * center.X - marginAxes, center.Y, 2 * center.X - paddingArrowY_OX, center.Y - paddingArrowY_OY); // OY
            place.DrawLine(axesPen, 2 * center.X - marginAxes, center.Y, 2 * center.X - paddingArrowY_OX, center.Y + paddingArrowY_OY);

            int lengthDegree = 25; // расстояние между делениями (в пикселах)
            int lengthHalfBar = 3; // половина длины штриха деления
            int maxValueForAxesX = 10; // максимальное значение по оси OX
            int maxValueForAxesY = 2; // максимальное значение по оси OY

            float firstDegreeX = (float)maxValueForAxesX / ((float)(center.X) / (float)lengthDegree); // цена деления по оси OX
            float firstDegreeY = (float)maxValueForAxesY / ((float)(center.Y) / (float)lengthDegree); // цена деления по оси OY

            int borderBar = 30; // граница отрисовки штриха
            int borderValues = borderBar + 25; // граница отрисовки значений
            int paddingValueX_OX = 9; // отступ значений OX относительно OX
            int paddingValueX_OY = 6; // относительно OY
            int paddingValueY_OX = 25; // отступ значений OY относительно OX
            int paddingValueY_OY = 6; // относительно OY

            /* Отрисовка значений по оси OX */
            for (int i = center.X, j = center.X, k = 1; i < 2 * center.X - borderBar; j -= lengthDegree, i += lengthDegree, k++)
            {
                place.DrawLine(axesPen, i, center.Y - lengthHalfBar, i, center.Y + lengthHalfBar);
                place.DrawLine(axesPen, j, center.Y - lengthHalfBar, j, center.Y + lengthHalfBar);

                if (i < 2 * center.X - borderValues)
                {
                    place.DrawString((k * firstDegreeX).ToString("0.0"), numberFont, signatureBrush, new PointF(i + lengthDegree + paddingValueX_OX, center.Y + paddingValueX_OY), signatureFormat);
                    place.DrawString(((k * firstDegreeX).ToString("0.0").ToString() + "-"), numberFont, signatureBrush, new PointF(j - lengthDegree + paddingValueX_OX, center.Y + paddingValueX_OY), signatureFormat);
                }
            }
            /* Отрисовка значений по оси OY */
            for (int i = center.Y, j = center.Y, k = 1; i < 2 * center.Y - borderBar; j -= lengthDegree, i += lengthDegree, k++)
            {
                place.DrawLine(axesPen, center.X - lengthHalfBar, i, center.X + lengthHalfBar, i);
                place.DrawLine(axesPen, center.X - lengthHalfBar, j, center.X + lengthHalfBar, j);

                if (i < 2 * center.Y - borderValues)
                {
                    place.DrawString((k * firstDegreeY).ToString("0.0").ToString(), numberFont, signatureBrush, new PointF(center.X + paddingValueY_OX, j - lengthDegree - paddingValueY_OY), signatureFormat);
                    place.DrawString((k * firstDegreeY).ToString("0.0") + "-", numberFont, signatureBrush, new PointF(center.X + paddingValueY_OX, i + lengthDegree - paddingValueY_OY), signatureFormat);
                }
            }

            const int numberPoint = 2 * 100; // количество точек, по которым строится график

            float[] coordinatesX = new float[numberPoint]; // массив координат точек по оси OX
            // заполняем массив координат точек по оси OX
            for (int i = 0; i < numberPoint / 2; i++)
            {
                coordinatesX[i] = (float)maxValueForAxesX / ((float)numberPoint / 2) * (i + 1) - (float)(maxValueForAxesX);
                coordinatesX[i + numberPoint / 2] = (float)maxValueForAxesX / ((float)numberPoint / 2) * (i + 1);
            }

            float[] coordinatesY = new float[numberPoint]; // массив координат точек по оси OY
            // заполняем массив координат точек по оси OY, вычисляя значение функции sin(x)/x
            for (int i = 0; i < numberPoint; i++)
            {
                if (coordinatesX[i] != 0)
                    coordinatesY[i] = (float)Math.Sin(coordinatesX[i]) / coordinatesX[i];
                else
                    coordinatesY[i] = 1;
            }

            // создаём массив точек, по которым будем строить график
            Point[] pointArray = new Point[numberPoint];
            // коэффициенты для перевода исходных координат точек в координаты окна
            float tempX = 1 / firstDegreeX * lengthDegree;
            float tempY = 1 / firstDegreeY * lengthDegree;
            // заполняем массив pointArray
            for (int i = 0; i < numberPoint; i++)
            {
                pointArray[i].X = center.X + (int)(coordinatesX[i] * tempX);
                pointArray[i].Y = center.Y - (int)(coordinatesY[i] * tempY);
            }

            // строим график функции по точкам ломаными линии и сплайном
            place.DrawLines(functionLinesPen, pointArray);
            place.DrawCurve(functionCurvesPen, pointArray);

            pictureBox.Image = bmp; // выводим график функции на экран
        }

        private void FunctionButton_Click(object sender, EventArgs e)
        {
            pictureBox.Show(); // показываем элемент pictureBox (если ранее окно было скрыто)
            this.Resize += new System.EventHandler(this.MainForm_Resize); // включаем обработчик события отрисовки окна (для перерисовки графика функции)
            pictureBoxView.Hide(); // скрываем элемент pictureBoxView (если ранее было загружено изображение)
            
            DrawFunction(); // отрисовываем график функции
        }

        private void DrawImageRocket()
        {
            Bitmap bmp = new Bitmap(pictureBox.Width, pictureBox.Height); // создаём объект bmp, на котором будет отрисовывать ракету
            Graphics place = Graphics.FromImage(bmp); // создаём объект place, который будет выводить изображение из объекта bmp на экран
            place.Clear(pictureBox.BackColor); // очищаем экран
            
            // отступ от левого верхнего угла экрана
            int offsetX = 100;
            int offsetY = 50;

            // задаём координаты фона рисунка, цвет границы, заливку, толщину границы
            Point[] coordBorder =
            {
                new Point(0, 0),
                new Point(200, 0),
                new Point(200, 300),
                new Point(0, 300)
            };
            Color colorBorder = Color.Black;
            Color colorFillBorder = Color.FromArgb(0, 51, 102); // тёмно-синий цвет
            int thickBorder = 3;
            
            // задаём координаты основной части корабля, цвет границы, заливку, толщину границы
            Point[] coordBody =
            {
                new Point(75, 100),
                new Point(125, 100),
                new Point(125, 200),
                new Point(75, 200)
            };
            Color colorBody = Color.Black;
            Color colorFillBody = Color.FromArgb(93, 51, 0); // коричневый цвет
            int thickBody = 2;

            // задаём координаты головной части корабля, цвет границы, заливку, толщину границы
            Point[] coordHead = 
            {
                new Point(100, 35),

                new Point(125, 100),
                new Point(75, 100)
            };
            Color colorHead = Color.Black;
            Color colorFiilHead = Color.FromArgb(164, 198, 57); // салатовый цвет
            int thickHead = 2;
            
            // задаём координаты "основного огня", цвет границы, заливку, толщину границы
            Point[] coordMainFire =
            {
                new Point(75, 200),
                new Point(125, 200),

                new Point(150, 250),
                new Point(120, 220),
                new Point(125, 245),
                new Point(100, 220),
                new Point(75, 250),
                new Point(80, 215),
                new Point(50, 240)
            };
            Color colorMainFire = Color.Black;
            Color colorFillMainFire = Color.Yellow;
            int thickMainFire = 2;
            
            // задаём координаты "вторичного огня", цвет границы, заливку, толщину границы
            Point[] coordSecondaryFire =
            {
                new Point(75, 200),
                new Point(125, 200),

                new Point(135, 230),
                new Point(120, 210),
                new Point(115, 230),
                new Point(100, 210),
                new Point(85, 230),
                new Point(80, 205),
                new Point(68, 220)
            };
            Color colorSecondaryFire = Color.Black;
            Color colorFillSecondaryFire = Color.Orange;
            int thickMainSecondaryFire = 2;

            int lengthStar = 3; // длина полосы "звезды"
            // координаты звёзд
            Point[,] coordStars =
            {
                { new Point(24 - lengthStar, 20), new Point(24 + lengthStar, 20), new Point(24, 20 - lengthStar), new Point(24, 20 + lengthStar) },         // (24, 20)
                { new Point(170 - lengthStar, 110), new Point(170 + lengthStar, 110), new Point(170, 110 - lengthStar), new Point(170, 110 + lengthStar) }, // (170, 110)
                { new Point(130 - lengthStar, 270), new Point(130 + lengthStar, 270), new Point(130, 270 - lengthStar), new Point(130, 270 + lengthStar) }, // (130, 270)
                { new Point(150 - lengthStar, 50), new Point(150 + lengthStar, 50), new Point(150, 50 - lengthStar), new Point(150, 50 + lengthStar) },     // (150, 50)
                { new Point(185 - lengthStar, 250), new Point(185 + lengthStar, 250), new Point(185, 250 - lengthStar), new Point(185, 250 + lengthStar) }, // (185, 250)
                { new Point(20 - lengthStar, 240), new Point(20 + lengthStar, 240), new Point(20, 240 - lengthStar), new Point(20, 240 + lengthStar) },     // (20, 240)
                { new Point(45 - lengthStar, 150), new Point(45 + lengthStar, 150), new Point(45, 150 - lengthStar), new Point(45, 150 + lengthStar) }      // (47, 150)
            };
            int numberCoords = 4; // количество координат, определяющих "звезду"
            int numberStars = coordStars.Length / numberCoords; // количество "звёзд"
            Color colorStars = Color.White; // цвет звёзд
            int thickStars = 2; // толщина звезды

            // делаем отступ от левого верхнего окна приложения
            for (int i = 0; i < coordBorder.Length; i++)
                coordBorder[i].Offset(offsetX, offsetY);
            for (int i = 0; i < coordBody.Length; i++)
                coordBody[i].Offset(offsetX, offsetY);
            for (int i = 0; i < coordHead.Length; i++)
                coordHead[i].Offset(offsetX, offsetY);
            for (int i = 0; i < coordMainFire.Length; i++)
                coordMainFire[i].Offset(offsetX, offsetY);
            for (int i = 0; i < coordSecondaryFire.Length; i++)
                coordSecondaryFire[i].Offset(offsetX, offsetY);
            for (int i = 0; i < numberStars; i++)
                for (int j = 0; j < numberCoords; j++)
                    coordStars[i, j].Offset(offsetX, offsetY);

            // отрисовываем все части корабля        
            
            place.DrawPolygon(new Pen(colorBorder, thickBorder), coordBorder);
            place.FillPolygon(new SolidBrush(colorFillBorder), coordBorder);

            place.DrawPolygon(new Pen(colorBody, thickBody), coordBody);
            place.FillPolygon(new SolidBrush(colorFillBody), coordBody);

            place.DrawPolygon(new Pen(colorHead, thickHead), coordHead);
            place.FillPolygon(new SolidBrush(colorFiilHead), coordHead);

            place.DrawPolygon(new Pen(colorMainFire, thickMainFire), coordMainFire);
            place.FillPolygon(new SolidBrush(colorFillMainFire), coordMainFire);

            place.DrawPolygon(new Pen(colorSecondaryFire, thickMainSecondaryFire), coordSecondaryFire);
            place.FillPolygon(new SolidBrush(colorFillSecondaryFire), coordSecondaryFire);

            // отрисовываем звёзды
            for (int i = 0; i < numberStars; i++)
            {
                place.DrawLine(new Pen(colorStars, thickStars), coordStars[i, 0], coordStars[i, 1]);
                place.DrawLine(new Pen(colorStars, thickStars), coordStars[i, 2], coordStars[i, 3]);
            }
            
            pictureBox.Image = bmp; // отрисовываем ракету
        }

        private void DrawRocket()
        {
            Graphics place = pictureBox.CreateGraphics(); // создаём объект place, который будет выводить изображение на экран
            place.Clear(pictureBox.BackColor); // очищаем экран

            // отступ от левого верхнего угла экрана
            int offsetX = 100;
            int offsetY = 50;

            // задаём координаты фона рисунка, цвет границы, заливку, толщину границы
            Point[] coordBorder =
            {
                new Point(0, 0),
                new Point(200, 0),
                new Point(200, 300),
                new Point(0, 300)
            };
            Color colorBorder = Color.Black;
            Color colorFillBorder = Color.FromArgb(0, 51, 102); // тёмно-синий цвет
            int thickBorder = 3;
            
            // задаём координаты основной части корабля, цвет границы, заливку, толщину границы
            Point[] coordBody =
            {
                new Point(75, 100),
                new Point(125, 100),
                new Point(125, 200),
                new Point(75, 200)
            };
            Color colorBody = Color.Black;
            Color colorFillBody = Color.FromArgb(93, 51, 0); // коричневый цвет
            int thickBody = 2;

            // задаём координаты головной части корабля, цвет границы, заливку, толщину границы
            Point[] coordHead = 
            {
                new Point(100, 35),

                new Point(125, 100),
                new Point(75, 100)
            };
            Color colorHead = Color.Black;
            Color colorFiilHead = Color.FromArgb(164, 198, 57); // салатовый цвет
            int thickHead = 2;
            
            // задаём координаты "основного огня", цвет границы, заливку, толщину границы
            Point[] coordMainFire =
            {
                new Point(75, 200),
                new Point(125, 200),

                new Point(150, 250),
                new Point(120, 220),
                new Point(125, 245),
                new Point(100, 220),
                new Point(75, 250),
                new Point(80, 215),
                new Point(50, 240)
            };
            Color colorMainFire = Color.Black;
            Color colorFillMainFire = Color.Yellow;
            int thickMainFire = 2;
            
            // задаём координаты "вторичного огня", цвет границы, заливку, толщину границы
            Point[] coordSecondaryFire =
            {
                new Point(75, 200),
                new Point(125, 200),

                new Point(135, 230),
                new Point(120, 210),
                new Point(115, 230),
                new Point(100, 210),
                new Point(85, 230),
                new Point(80, 205),
                new Point(68, 220)
            };
            Color colorSecondaryFire = Color.Black;
            Color colorFillSecondaryFire = Color.Orange;
            int thickMainSecondaryFire = 2;

            int lengthStar = 3; // длина полосы "звезды"
            // координаты звёзд
            Point[,] coordStars =
            {
                { new Point(24 - lengthStar, 20), new Point(24 + lengthStar, 20), new Point(24, 20 - lengthStar), new Point(24, 20 + lengthStar) },         // (24, 20)
                { new Point(170 - lengthStar, 110), new Point(170 + lengthStar, 110), new Point(170, 110 - lengthStar), new Point(170, 110 + lengthStar) }, // (170, 110)
                { new Point(130 - lengthStar, 270), new Point(130 + lengthStar, 270), new Point(130, 270 - lengthStar), new Point(130, 270 + lengthStar) }, // (130, 270)
                { new Point(150 - lengthStar, 50), new Point(150 + lengthStar, 50), new Point(150, 50 - lengthStar), new Point(150, 50 + lengthStar) },     // (150, 50)
                { new Point(185 - lengthStar, 250), new Point(185 + lengthStar, 250), new Point(185, 250 - lengthStar), new Point(185, 250 + lengthStar) }, // (185, 250)
                { new Point(20 - lengthStar, 240), new Point(20 + lengthStar, 240), new Point(20, 240 - lengthStar), new Point(20, 240 + lengthStar) },     // (20, 240)
                { new Point(45 - lengthStar, 150), new Point(45 + lengthStar, 150), new Point(45, 150 - lengthStar), new Point(45, 150 + lengthStar) }      // (47, 150)
            };
            int numberCoords = 4; // количество координат, определяющих "звезду"
            int numberStars = coordStars.Length / numberCoords; // количество "звёзд"
            Color colorStars = Color.White; // цвет звёзд
            int thickStars = 2; // толщина звезды

            // делаем отступ от левого верхнего окна приложения
            for (int i = 0; i < coordBorder.Length; i++)
                coordBorder[i].Offset(offsetX, offsetY);
            for (int i = 0; i < coordBody.Length; i++)
                coordBody[i].Offset(offsetX, offsetY);
            for (int i = 0; i < coordHead.Length; i++)
                coordHead[i].Offset(offsetX, offsetY);
            for (int i = 0; i < coordMainFire.Length; i++)
                coordMainFire[i].Offset(offsetX, offsetY);
            for (int i = 0; i < coordSecondaryFire.Length; i++)
                coordSecondaryFire[i].Offset(offsetX, offsetY);
            for (int i = 0; i < numberStars; i++)
                for (int j = 0; j < numberCoords; j++)
                    coordStars[i, j].Offset(offsetX, offsetY);

            // отрисовываем все части корабля        
            
            place.DrawPolygon(new Pen(colorBorder, thickBorder), coordBorder);
            place.FillPolygon(new SolidBrush(colorFillBorder), coordBorder);

            place.DrawPolygon(new Pen(colorBody, thickBody), coordBody);
            place.FillPolygon(new SolidBrush(colorFillBody), coordBody);

            place.DrawPolygon(new Pen(colorHead, thickHead), coordHead);
            place.FillPolygon(new SolidBrush(colorFiilHead), coordHead);

            place.DrawPolygon(new Pen(colorMainFire, thickMainFire), coordMainFire);
            place.FillPolygon(new SolidBrush(colorFillMainFire), coordMainFire);

            place.DrawPolygon(new Pen(colorSecondaryFire, thickMainSecondaryFire), coordSecondaryFire);
            place.FillPolygon(new SolidBrush(colorFillSecondaryFire), coordSecondaryFire);

            // отрисовываем звёзды
            for (int i = 0; i < numberStars; i++)
            {
                place.DrawLine(new Pen(colorStars, thickStars), coordStars[i, 0], coordStars[i, 1]);
                place.DrawLine(new Pen(colorStars, thickStars), coordStars[i, 2], coordStars[i, 3]);
            }
        }

        private void RocketButton_Click(object sender, EventArgs e)
        {
            pictureBox.Show(); // показываем элемент pictureBox (для вывода рисунка в окно приложения)
            this.Resize -= new System.EventHandler(this.MainForm_Resize); // отключаем обработчик события отрисовки окна (для перерисовки графика функции)
            pictureBoxView.Hide(); // скрываем объект pictureBoxView()
            
            DrawImageRocket(); // отрисовываем ракету
        }

        private void DrawAnimation()
        {
            DrawRocket(); // отрисовываем ракету

            Graphics place = pictureBox.CreateGraphics(); // создаём объект place, который будет выводить изображение на экран
            
            // отступ от левого верхнего угла экрана
            int offsetX = 100;
            int offsetY = 50;
            
            Color colorFillBorder = Color.FromArgb(0, 51, 102); // тёмно-синий цвет
            int delay = 200; // задержка кадра
            int maxLengthStar = 15; // максимальная длина полосы "звезды"

            int lengthStar = 3; // длина полосы "звезды"
            // координаты звёзд
            Point[,] coordStars =
            {
                { new Point(24 - lengthStar, 20), new Point(24 + lengthStar, 20), new Point(24, 20 - lengthStar), new Point(24, 20 + lengthStar) },         // (24, 20)
                { new Point(170 - lengthStar, 110), new Point(170 + lengthStar, 110), new Point(170, 110 - lengthStar), new Point(170, 110 + lengthStar) }, // (170, 110)
                { new Point(130 - lengthStar, 270), new Point(130 + lengthStar, 270), new Point(130, 270 - lengthStar), new Point(130, 270 + lengthStar) }, // (130, 270)
                { new Point(150 - lengthStar, 50), new Point(150 + lengthStar, 50), new Point(150, 50 - lengthStar), new Point(150, 50 + lengthStar) },     // (150, 50)
                { new Point(185 - lengthStar, 250), new Point(185 + lengthStar, 250), new Point(185, 250 - lengthStar), new Point(185, 250 + lengthStar) }, // (185, 250)
                { new Point(20 - lengthStar, 240), new Point(20 + lengthStar, 240), new Point(20, 240 - lengthStar), new Point(20, 240 + lengthStar) },     // (20, 240)
                { new Point(45 - lengthStar, 150), new Point(45 + lengthStar, 150), new Point(45, 150 - lengthStar), new Point(45, 150 + lengthStar) }      // (47, 150)
            };
            int numberCoords = 4; // количество координат, определяющих "звезду"
            int numberStars = coordStars.Length / numberCoords; // количество "звёзд"
            Color colorStars = Color.White; // цвет звёзд
            int thickStars = 2; // толщина звезды
            
            // отступаем от левого верхнего угла экрана
            for (int i = 0; i < numberStars; i++)
                for (int j = 0; j < numberCoords; j++)
                    coordStars[i, j].Offset(offsetX, offsetY);

            // "Убираем" звёзды
            for (int i = 0; i < numberStars; i++)
            {
                place.DrawLine(new Pen(colorFillBorder, thickStars), coordStars[i, 0], coordStars[i, 1]);
                place.DrawLine(new Pen(colorFillBorder, thickStars), coordStars[i, 2], coordStars[i, 3]);
            }

            Thread.Sleep(delay); // создаём задержку для более плавной анимации

            // "Увеличиваем" звёзды
            for (int i = lengthStar; i < maxLengthStar; i++)
            {
                // перевычисляем координаты звёзд
                for (int j = 0; j < numberStars; j++)
                {
                    coordStars[j, 0].X -= 1;
                    coordStars[j, 1].X += 1;
                    coordStars[j, 2].Y -= 1;
                    coordStars[j, 3].Y += 1;
                }

                // перерисовываем звёзды
                for (int j = 0; j < numberStars; j++)
                {
                    place.DrawLine(new Pen(colorStars, thickStars), coordStars[j, 0], coordStars[j, 1]);
                    place.DrawLine(new Pen(colorStars, thickStars), coordStars[j, 2], coordStars[j, 3]);
                }

                Thread.Sleep(delay); // создаём задержку для более плавной анимации
            }

            DrawRocket(); // отрисовываем ракету
        }

        private void AnimationButton_Click(object sender, EventArgs e)
        {
            pictureBox.Show(); // показываем элемент pictureBox (для вывода рисунка в окно приложения)
            this.Resize -= new System.EventHandler(this.MainForm_Resize); // отключаем обработчик события отрисовки окна (для перерисовки графика функции)
            pictureBoxView.Hide(); // скрываем объект pictureBoxView()

            Thread stream = new Thread(DrawAnimation); // создаём поток и отправляем в него функцию отрисовки анимации ракеты
            stream.Start(); // запускаем поток
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            this.Resize -= new System.EventHandler(this.MainForm_Resize); // отключаем обработчик события отрисовки окна (для перерисовки графика функции)

            ImageFormat image = ImageFormat.Jpeg; // задаём формат, имя и путь сохранения картинки 
            
            // при вызове окна сохранения выбираем формат изображения
            switch (saveFileDialog.FilterIndex)
            {
                case 0: image = ImageFormat.Bmp; break;
                case 1: image = ImageFormat.Jpeg; break;
                case 2: image = ImageFormat.Png; break;
            }

            Bitmap bmp = (Bitmap)pictureBox.Image; // создаём объект bmp, куда загружаем изображение из окна программы
            if (saveFileDialog.ShowDialog() == DialogResult.OK) // если сохранение файла прошло успешно
                bmp.Save(saveFileDialog.FileName, image); // сохраняем изображение в файл
        }

        private void LoadButton_Click(object sender, EventArgs e)
        {
            this.Resize -= new System.EventHandler(this.MainForm_Resize); // отключаем обработчик события отрисовки окна (для перерисовки графика функции)
            pictureBox.Hide(); // отключаем элемент pictureBox
            pictureBoxView.Show(); // показываем элемент pictureBoxView (для вывода изображения в окно приложения)

            if (openFileDialog.ShowDialog() == DialogResult.OK) // если открытие файла прошло успешно
                pictureBoxView.Image = Image.FromFile(openFileDialog.FileName); // выводим изображение на экран
        }
    }
}
