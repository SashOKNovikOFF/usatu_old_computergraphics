﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SaveLoadImages
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            pictureBox1.Hide();
            Graphics myGraphic = this.CreateGraphics();
            Pen redPen = new Pen(Color.Red, 6);
            SolidBrush yellowBrush = new SolidBrush(Color.Yellow);
            myGraphic.DrawRectangle(redPen, 100, 100, 50, 100);
            Pen greenPen = new Pen(Color.Green, 3);
            myGraphic.DrawEllipse(greenPen, 200, 100, 200, 100);
            myGraphic.FillEllipse(yellowBrush, 200, 100, 200, 100);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ImageFormat image = ImageFormat.Jpeg;
            saveFileDialog1.ShowDialog();
            switch (saveFileDialog1.FilterIndex)
            {
                case 0: image = ImageFormat.Bmp; break;
                case 1: image = ImageFormat.Jpeg; break;
                case 2: image = ImageFormat.Png; break;
            }
            Rectangle bounds = this.Bounds;
            Bitmap bitmap = new Bitmap(bounds.Width, bounds.Height);
            Graphics myGraphic = Graphics.FromImage(bitmap);
            myGraphic.CopyFromScreen(new Point(bounds.Left, bounds.Top), Point.Empty, bounds.Size);
            bitmap.Save(saveFileDialog1.FileName, image);
            pictureBox1.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            pictureBox1.Image = Image.FromFile(openFileDialog1.FileName);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            pictureBox1.Hide();
            Graphics myGraphic = this.CreateGraphics();
            LinearGradientBrush myBrush = new LinearGradientBrush(ClientRectangle, Color.Red, Color.Yellow, System.Drawing.Drawing2D.LinearGradientMode.Horizontal);
            Font tahoma = new Font("Tahoma", 24, FontStyle.Regular);
            myGraphic.DrawString("Text1", tahoma, myBrush, new RectangleF(250, 130, 100, 200));
        }
    }
}
