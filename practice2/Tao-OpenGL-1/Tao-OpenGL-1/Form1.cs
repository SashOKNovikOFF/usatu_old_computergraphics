﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Tao.FreeGlut; // библиотека FreeGlut
using Tao.OpenGl; // библиотека OpenGL
using Tao.Platform.Windows; // библиотека для работы с SimpleOpenGLControl

namespace Tao_OpenGL_1
{
    public partial class MainForm : Form
    {
        double screenWidth, screenHeight; // размеры окна (ширина, высота)
        
        // отношения сторон окна визуализации (для мыши)
        private float ratioX;
        private float ratioY;
        
        private float[,] valuesFunction; // двумерный массив со значениями x, y точек графика
        private int elementsCount = 0; // кол-во элементов в массиве
        private bool notCalc = true; // массив valuesFunction ещё не заполнен
        private int pointPosition = 0; // номер ячейки для текущей координаты красной точки
        float redLineX, redLineY; // переменные для построения "красных" линий
        float mouseCoordX = 0, mouseCoordY = 0; // координаты мыши
        
        private static int numberPoint = 300; // количество точек, хранящихся в массиве valuesFunction
        private int minBorderX = -10, maxBorderX = 10; // границы аргумента x
        private int minBorderY = -2, maxBorderY = 2; // границы аргумента y

        // функция визуализации текста
        private void PrintText2D(float x, float y, string text)
        {
            /* The GL maintains a 3D position in window coordinates. This position, called the raster position, is used to position pixel and bitmap write operations. It is maintained with subpixel accuracy. */
            Gl.glRasterPos2f(x, y); // устанавливаем место вывода растровых символов

            foreach (char drawChar in text)
            {
                // визуализируем каждый символ на экране
                Glut.glutBitmapCharacter(Glut.GLUT_BITMAP_8_BY_13, drawChar);
            }
        }

        // функция, вычисляющая координаты графика функции
        private void calculateFunction()
        {
            float x = 0, y = 0; // временные переменные
            valuesFunction = new float[numberPoint, 2]; // массив из 300 точек
            elementsCount = 0; // счётчик элементов массива
            for (x = minBorderX; x < maxBorderX; x += (float)(maxBorderX - minBorderX) / (float)numberPoint) // вычисление y для текущего x
            {
                if (elementsCount >= numberPoint) // если массив valuesFunction заполнен
                    break;
                if (x != 0) // значение функции y = f(x)
                    y = (float)Math.Sin(x) / x;
                else
                    y = 1;
                valuesFunction[elementsCount, 0] = x; // заполняем очередной элемент массива аргументов функции
                valuesFunction[elementsCount, 1] = y; // заполняем очередной элемент массива значением функции
                elementsCount++; // увеличиваем счётчик
            }
            notCalc = true; // массив valuesFunction заполнен
        }
        // функция отрисовки графика функции
        private void DrawGraphic()
        {
            if (notCalc) // если значения функции не вычислялись, вычислить
                calculateFunction();

            /* glBegin and glEnd delimit the vertices that define a primitive or a group of like primitives. */
            /* Draws a connected group of line segments from the first vertex to the last. */
            Gl.glBegin(Gl.GL_LINE_STRIP);

            Gl.glVertex2d(valuesFunction[0, 0], valuesFunction[0, 1]); // рисуем начальную точку

            for (int i = 1; i < elementsCount; i += 2)
            {
                Gl.glVertex2d(valuesFunction[i, 0], valuesFunction[i, 1]); // рисуем дальнейшие вершины
            }

            // заканчиваем отрисовку линии графика
            Gl.glEnd();

            Gl.glPointSize(5); // устанавливаем размер точек
            Gl.glColor3f(255, 0, 0); // устанавливаем красный цвет
            /* Treats each pair of vertices as an independent line segment. */
            Gl.glBegin(Gl.GL_POINTS);
            Gl.glVertex2d(valuesFunction[pointPosition, 0], valuesFunction[pointPosition, 1]); // рисуем красную точку
            Gl.glEnd();
            Gl.glPointSize(1); // возвращаем размер точек в исходное состояние
        }

        private void DrawFunction()
        {
            /* glClear sets the bitplane area of the window to values previously selected by glClearColor, glClearIndex, glClearDepth, glClearStencil, and glClearAccum. */
            /* Indicates the buffers currently enabled for color writing. */
            /* Indicates the depth buffer. */
            Gl.glClear(Gl.GL_COLOR_BUFFER_BIT | Gl.GL_DEPTH_BUFFER_BIT); // правильно ли я понимаю, что Gl.GL_DEPTH_BUFFER_BIT не требуется при работе в двхумерном пространстве?
            Gl.glLoadIdentity(); // очищение текущей матрицы
            Gl.glColor3f(0, 0, 0); // устанавливаем чёрный цвет
            /* glPushMatrix pushes the current matrix stack down by one, duplicating the current matrix. That is, after a glPushMatrix call, the matrix on top of the stack is identical to the one below it. */
            Gl.glPushMatrix(); // помещаем состояние матриц в стек матриц
            /* glTranslate — multiply the current matrix by a translation matrix. */
            
            // переменные, хранящие значения смещения матрицы
            int offsetX, offsetY;
            offsetX = 11;
            offsetY = 6;
            
            Gl.glTranslated(offsetX, offsetY, 0); // выполняем перемещение в пространстве
            
            Gl.glBegin(Gl.GL_POINTS); // рисуем точки
            // создаём сетку с помощью двойного цикла
            for (float i = minBorderX; i <= maxBorderX; i++)
                for (int j = minBorderY; j <= maxBorderY; j++)
                    Gl.glVertex2d(i, j);
            Gl.glEnd(); // заканчинваем рисовать точки

            /* Treats each pair of vertices as an independent line segment. */
            Gl.glBegin(Gl.GL_LINES);
                Gl.glVertex2d(minBorderX, 0); // ось OX
                Gl.glVertex2d(maxBorderX, 0);
                Gl.glVertex2d(0, minBorderY); // ось OY
                Gl.glVertex2d(0, maxBorderY);
                Gl.glVertex2d(maxBorderX, 0); // стрелка оси OX
                Gl.glVertex2d(maxBorderX - 0.5f, 0.2f);
                Gl.glVertex2d(maxBorderX, 0);
                Gl.glVertex2d(maxBorderX - 0.5f, -0.2f);
                Gl.glVertex2d(0, maxBorderY); // стрелка оси OY
                Gl.glVertex2d(0.2f, maxBorderY - 0.5f);
                Gl.glVertex2d(0, maxBorderY);
                Gl.glVertex2d(-0.2f, maxBorderY - 0.5f);
            Gl.glEnd();
            PrintText2D(-0.5f, -0.6f, "0");
            PrintText2D(maxBorderX - 0.3f, -0.8f, "X");
            PrintText2D(0.4f, maxBorderY - 0.2f, "Y");

            DrawGraphic(); // рисуем график функции
            Gl.glPopMatrix(); // возвращаем матрицу из стека

            if ((redLineX >= 1) && (redLineX <= (maxBorderX + offsetX)) && (redLineY >= 4) && (redLineY <= (maxBorderY + offsetY)))
            {
                PrintText2D(redLineX + 0.2f, redLineY + 0.4f, "[ x: " + Math.Round((redLineX - offsetX), 2).ToString() + "; y " + Math.Round((redLineY - offsetY), 2).ToString() + "]");
                Gl.glColor3f(255, 0, 0); // устанавливаем красный цвет
                Gl.glBegin(Gl.GL_LINES); // рисуем линии от курсора мыши до коорд. осей
                    Gl.glVertex2d(redLineX, offsetY);
                    Gl.glVertex2d(redLineX, redLineY);
                    Gl.glVertex2d(offsetX, redLineY);
                    Gl.glVertex2d(redLineX, redLineY);
                Gl.glEnd();
            }

            /* Different GL implementations buffer commands in several different locations, including network buffers and the graphics accelerator itself. glFlush empties all of these buffers, causing all issued commands to be executed as quickly as they are accepted by the actual rendering engine. */
            Gl.glFlush(); // дожидаемся визуализации кадра
            AnT.Invalidate(); // сигнал для обновления AnT, реализующего визуализацию
        }

        public MainForm()
        {
            InitializeComponent();
            AnT.InitializeContexts(); // инициализируем работу AnT
        }

        private void PointInGrap_Tick(object sender, EventArgs e)
        {
            // если дошли до конца массива
            if (pointPosition == elementsCount - 1)
                pointPosition = 0; // переходим к началу массива

            DrawFunction(); // функция визуализации всего графика

            pointPosition++; // переходим к следующему элементу
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Glut.glutInit(); // инициализируем библиотеку glut
            Glut.glutInitDisplayMode(Glut.GLUT_RGB | Glut.GLUT_DOUBLE); // инициализируем режим отображения (RGBA и двойная буферизация)
            Gl.glClearColor(255, 255, 255, 1); // установка цвета очистки экрана (белый цвет)

            /* glViewport specifies the affine transformation of x and y from normalized device coordinates to window coordinates. */
            /* glMatrixMode sets the current matrix mode. Applies subsequent matrix operations to the projection matrix stack. */
            /* glLoadIdentity replaces the current matrix with the identity matrix. */
            Gl.glViewport(0, 0, AnT.Width, AnT.Height); // установка порта вывода
            Gl.glMatrixMode(Gl.GL_PROJECTION); // активация проекционной матрицы
            Gl.glLoadIdentity(); // очистка матрицы

            // коэффициенты, влияющие на длину и ширину области проекции
            float sizeRatioX = 12.0f;
            float sizeRatioY = 12.0f;

            /* gluOrtho2D sets up a two-dimensional orthographic viewing region. */
            if ((float)AnT.Width <= (float)AnT.Height) // определение параметров настройки проекции
            {
                screenWidth = sizeRatioX;
                screenHeight = sizeRatioY * (float)AnT.Height / (float)AnT.Width;

                Glu.gluOrtho2D(0.0, screenWidth, 0.0, screenHeight);
            }
            else
            {
                screenWidth = sizeRatioX * (float)AnT.Width / (float)AnT.Height;
                screenHeight = sizeRatioY;

                Glu.gluOrtho2D(0.0, screenWidth, 0.0, screenHeight);
            }

            // сохранение коэффициентов для перевода координат
            ratioX = (float)screenHeight / (float)AnT.Height;
            ratioY = (float)screenWidth / (float)AnT.Width;

            /* Applies subsequent matrix operations to the modelview matrix stack. */
            Gl.glMatrixMode(Gl.GL_MODELVIEW); /// установка объектно-видовой матрицы

            PointInGrap.Start(); // включение таймера для визуализации сцены
        }

        private void AnT_MouseMove(object sender, MouseEventArgs e)
        {
            // сохраняем координаты мыши
            mouseCoordX = e.X;
            mouseCoordY = e.Y;

            // параметры "красных" линий для дорисвоки от мыши к коорд. осям
            redLineX = ratioX * e.X;
            redLineY = (float)(screenHeight - ratioY * e.Y);
        }
    }
}
