﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Tao.OpenGl;
using Tao.FreeGlut;
using Tao.Platform.Windows;

namespace LabWork_4
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            openGlControl.InitializeContexts(); // инициализация компонента openGlControl
        }

        float angleRotation = 0; // переменная, отвечающая за угол поворота вокруг поверхности

        // структура "Вешина"
        struct Vertex
        {
            public float x, y, z; // координаты вершины
            public float nx, ny, nz; // координаты нормали
        }

        // класс "Функционально заданная поверхность"
        class FunctionSurface
        {
            // функция, вычисляющая значение функции z(x, y)
            public double Saddle(double x, double y)
            {
                return Math.Pow(x, 3) - 3 * x * Math.Pow(y, 2);
            }

            // функция, вычисляющая значение функции F(x, y, z)
            public double F(double x, double y, double z)
            {
                return Saddle(x, y) - z;
            }

            // функция, вычисляющая координаты вершины и её нормаль
            public Vertex CalculateVertex(double x, double y)
            {
                Vertex result = new Vertex();

                double z = Saddle(x, y); // находим значение z(x, y) в заданной точке

                // находим координаты градиента функции F(x, y, z) в заданной точке
                double dfdx = 3 * (Math.Pow(y, 2) - Math.Pow(x, 2));
                double dfdy = 6 * x * y;
                double dfdz = 1;

                // находим длину, для нормализации вектора нормали
                double length = 1 / Math.Sqrt(dfdx * dfdx + dfdy * dfdy + dfdz * dfdz);

                // координаты вершины
                result.x = (float)x;
                result.y = (float)y;
                result.z = (float)z;

                // координаты нормали
                result.nx = (float)(dfdx * length);
                result.ny = (float)(dfdy * length);
                result.nz = (float)(dfdz * length);

                return result;
            }
        }

        // функция, устанавливающая материал поверхности
        private void SetupMaterial()
        {
            int side = Gl.GL_FRONT; // GL_FRONT for front-facing polygons.

            float[] diffuse = new float[] { 0.0f, 0.5f, 0.0f, 1 };
            float[] ambient = new float[] { 0.0f, 0.2f, 0.0f, 1 };
            float[] specular = new float[] { 0.3f, 0.2f, 0.3f, 1 };
            float shininess = 1;

            // glMaterial — specify material parameters for the lighting model.
            // В модели Фонга для глаза наблюдателя интенсивность зеркально отраженного луча зависит от угла
            // между идеально отраженным лучом и направлением к наблюдателю, а также от длины волны.
            Gl.glMaterialfv(side, Gl.GL_DIFFUSE, diffuse); // Диффузное отражение - свет как бы проникает под поверхность объекта, поглощается, потом равномерно излучается во всех направлениях.
            Gl.glMaterialfv(side, Gl.GL_AMBIENT, ambient); // Фоновая компонента — грубое приближение лучей света, рассеянных соседними объектами и затем достигших заданной точки.
            Gl.glMaterialfv(side, Gl.GL_SPECULAR, specular); // Зеркальное отражение - происходит от внешней поверхности, интенсивность его неоднородна, видимый максимум освещенности зависит от положения глаза наблюдателя.
            Gl.glMaterialfv(side, Gl.GL_SHININESS, ref shininess); // Блеск.
        }

        // функция, устанавливающая освещение поверхности
        private void SetupLight()
        {
            int light = Gl.GL_LIGHT0;

            float[] direction = new float[] { 2, 2, 2, 0 };
            float[] diffuse = new float[] { 0.5f, 0.5f, 0.5f, 1 };
            float[] ambient = new float[] { 0.5f, 0.5f, 0.5f, 1 };
            float[] specular = new float[] { 0.0f, 0.0f, 0.0f, 1 };

            // glLight sets the values of individual light source parameters.
            Gl.glLightfv(light, Gl.GL_POSITION, direction); // позиция источника света
            Gl.glLightfv(light, Gl.GL_DIFFUSE, diffuse); // Диффузное отражение - свет как бы проникает под поверхность объекта, поглощается, потом равномерно излучается во всех направлениях.
            Gl.glLightfv(light, Gl.GL_AMBIENT, ambient); // Фоновая компонента — грубое приближение лучей света, рассеянных соседними объектами и затем достигших заданной точки.
            Gl.glLightfv(light, Gl.GL_SPECULAR, specular); // Зеркальное отражение - происходит от внешней поверхности, интенсивность его неоднородна, видимый максимум освещенности зависит от положения глаза наблюдателя.

            // GL_LIGHTING - If enabled and no vertex shader is active, use the current lighting parameters to compute the vertex color or index.
            Gl.glEnable(Gl.GL_LIGHTING);
            Gl.glEnable(light);
        }

        // функция, устанавливающая камеру
        private void SetupCamera()
        {
            float speedRotation = 0.5f; // переменная, отвечающая за скорость вращения камеры

            Gl.glLoadIdentity(); // очистка объектно-видовой матирцы

            angleRotation = (angleRotation + speedRotation) % 360; // угол поворта камеры
            // gluLookAt creates a viewing matrix derived from an eye point, a reference point indicating the center of the scene, and an UP vector.
            Glu.gluLookAt(12, 12, 8, 0, 0, 0, 0, 0, 1);
            Gl.glRotatef(angleRotation, 0, 0, 1);
        }

        // функция, отрисовывающая функционально заданную поверхность
        private void DrawSurface()
        {
            // переменные, связанные с работой дисплейного списка
            int displayList = 0;
            int columns = 50; // количество лент из треугольников по горизонтали
            int rows = 50; // количество лент из треугольников по вертикали

            // границы отображения поверхности
            float xMin = -15;
            float xMax = 15;
            float yMin = -15;
            float yMax = 15;

            // масштаб по осям OX, OY и OZ
            float scaleXY = 0.5f;
            float scaleZ = 0.0005f;

            // настройка проекции
            float zNear = 1f;
            float zFar = 40;
            float fieldView = 60;
            float aspect = (float)openGlControl.Width / (float)openGlControl.Height;
            FunctionSurface function = new FunctionSurface();

            // очистка экрана и буферов цвета и глубины
            Gl.glClearColor(0, 0, 0, 0);
            Gl.glClear(Gl.GL_COLOR_BUFFER_BIT | Gl.GL_DEPTH_BUFFER_BIT);

            // установка освещения, материала и камеры
            SetupLight();
            SetupMaterial();
            SetupCamera();

            // установка отрисовки полигонов, составляющих поверхность
            switch (comboBox.SelectedIndex)
            {
                case 0:
                    Gl.glPolygonMode(Gl.GL_FRONT, Gl.GL_POINT);
                    break;
                case 1:
                    Gl.glPolygonMode(Gl.GL_FRONT, Gl.GL_LINE);
                    break;
                case 2:
                    Gl.glPolygonMode(Gl.GL_FRONT, Gl.GL_FILL);
                    break;
            }

            // установка проекции
            Gl.glMatrixMode(Gl.GL_PROJECTION);
            Gl.glLoadIdentity();
            Glu.gluPerspective(fieldView, aspect, zNear, zFar);
            Gl.glMatrixMode(Gl.GL_MODELVIEW);
            Gl.glPushMatrix();
            
            // glScale — multiply the current matrix by a general scaling matrix.
            Gl.glScalef(scaleXY, scaleXY, scaleZ); // настройка масштаба

            if (displayList == 0)
            {
                // glGenLists — generate a contiguous set of empty display lists.
                displayList = Gl.glGenLists(1); // создание дисплейного листа
                // glNewList — create or replace a display list.
                Gl.glNewList(displayList, Gl.GL_COMPILE); // Commands are merely compiled.

                // малые приращения dx и dy для построения сетки из треугольников
                float dx = (xMax - xMin) / (columns - 1);
                float dy = (yMax - yMin) / (rows - 1);

                float y = yMax;

                // построение сетки из треугольников
                Gl.glBegin(Gl.GL_TRIANGLE_STRIP);
                for (int row = rows; row > 1; --row, y -= dy)
                {
                    float x;
                    if (((rows - row + 1) % 2) != 0)
                        x = xMin;
                    else
                        x = xMax;

                    Vertex v0 = new Vertex();
                    Vertex v1 = new Vertex();
                    for (int column = 1; column <= columns; ++column)
                    {
                        // смена направления обхода при построении сетки
                        if (((rows - row + 1) % 2) != 0)
                        {
                            v0 = function.CalculateVertex(x, y);
                            v1 = function.CalculateVertex(x, y - dy);
                        }
                        else
                        {
                            v0 = function.CalculateVertex(x, y - dy);
                            v1 = function.CalculateVertex(x, y);
                        }

                        // установка нормалей и вершин
                        Gl.glNormal3f(v0.nx, v0.ny, v0.nz);
                        Gl.glVertex3f(v0.x, v0.y, v0.z);

                        Gl.glNormal3f(v1.nx, v1.ny, v1.nz);
                        Gl.glVertex3f(v1.x, v1.y, v1.z);

                        // смена направления обхода при построении сетки
                        if ((rows - row + 1) % 2 != 0)
                            x += dx;
                        else
                            x -= dx;
                    }

                    // смена направления обхода при построении сетки
                    if (x <= xMin)
                        x = xMin;
                    else
                        if (x >= xMax)
                            x = xMax;
                    if (row > 1)
                    {
                        v0 = function.CalculateVertex(x, y - dy);
                        v1 = function.CalculateVertex(x, y - 2 * dy);

                        Gl.glNormal3f(v0.nx, v0.ny, v0.nz);
                        Gl.glVertex3f(v0.x, v0.y, v0.z);

                        Gl.glNormal3f(v1.nx, v1.ny, v1.nz);
                        Gl.glVertex3f(v1.x, v1.y, v1.z);
                    }
                }
                Gl.glEnd();
                Gl.glEndList();
            }
            Gl.glCallList(displayList);
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            // инициализация Glut
            Glut.glutInit();
            Glut.glutInitDisplayMode(Glut.GLUT_RGB | Glut.GLUT_DOUBLE | Glut.GLUT_DEPTH);

            // glEnable and glDisable enable and disable various capabilities.
            // glCullFace specifies whether front- or back-facing facets are culled (as specified by mode) when facet culling is enabled.
            // glFrontFace — define front- and back-facing polygons.
            Gl.glEnable(Gl.GL_CULL_FACE); // If enabled, cull polygons based on their winding in window coordinates.
            Gl.glCullFace(Gl.GL_BACK); // Specifies whether front- or back-facing facets are candidates for culling.
            Gl.glFrontFace(Gl.GL_CCW); // Passing GL_CCW to mode selects counterclockwise polygons as front-facing.

            renderTimer.Start(); // запуск таймера
        }

        private void renderTimer_Tick(object sender, EventArgs e)
        {
            // отрисовка поверхности
            DrawSurface();
            openGlControl.Invalidate();
        }
    }
}
