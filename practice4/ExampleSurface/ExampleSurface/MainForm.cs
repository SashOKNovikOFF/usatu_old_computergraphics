﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Tao.OpenGl;
using Tao.FreeGlut;
using Tao.Platform.Windows;

namespace ExampleSurface
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            openGlControl.InitializeContexts();
        }

        float angleRotation = 0;

        struct Vertex
        {
            public float x, y, z;
            public float nx, ny, nz;
        }

        class FunctionSurface
        {
            public double Sinc(double x)
            {
                return (Math.Abs(x) < 1e-7) ? 1 : Math.Sin(x) / x;
            }

            public double F(double x, double y, double z)
            {
                double r = Math.Sqrt(x * x + y * y);
                return Sinc(r) - z;
            }

            public Vertex CalculateVertex(double x, double y)
            {
                Vertex result = new Vertex();

                double r = Math.Sqrt(x * x + y * y);
                double z = Sinc(r);

                double delta = 1e-7;

                float function = (float)F(x, y, z);

                double dfdx = -(F(x + delta, y, z) - function) / delta;
                double dfdy = -(F(x, y + delta, z) - function) / delta;
                double dfdz = 1;

                double length = 1 / Math.Sqrt(dfdx * dfdx + dfdy * dfdy + dfdz * dfdz);

                result.x = (float)x;
                result.y = (float)y;
                result.z = (float)z;

                result.nx = (float)(dfdx * length);
                result.ny = (float)(dfdy * length);
                result.nz = (float)(dfdz * length);

                return result;
            }
        }

        private void SetupMaterial()
        {
            int side = Gl.GL_FRONT; // GL_FRONT for front-facing polygons.

            float[] diffuse = new float[] { 0.0f, 0.5f, 0.0f, 1 };
            float[] ambient = new float[] { 0.0f, 0.2f, 0.0f, 1 };
            float[] specular = new float[] { 0.3f, 0.2f, 0.3f, 1 };
            float shininess = 1;

            // glMaterial — specify material parameters for the lighting model.
            // В модели Фонга для глаза наблюдателя интенсивность зеркально отраженного луча зависит от угла
            // между идеально отраженным лучом и направлением к наблюдателю, а также от длины волны.
            Gl.glMaterialfv(side, Gl.GL_DIFFUSE, diffuse); // Диффузное отражение - свет как бы проникает под поверхность объекта, поглощается, потом равномерно излучается во всех направлениях.
            Gl.glMaterialfv(side, Gl.GL_AMBIENT, ambient); // Фоновая компонента — грубое приближение лучей света, рассеянных соседними объектами и затем достигших заданной точки.
            Gl.glMaterialfv(side, Gl.GL_SPECULAR, specular); // Зеркальное отражение - происходит от внешней поверхности, интенсивность его неоднородна, видимый максимум освещенности зависит от положения глаза наблюдателя.
            Gl.glMaterialfv(side, Gl.GL_SHININESS, ref shininess); // Блеск.
        }

        private void SetupLight()
        {
            int light = Gl.GL_LIGHT0;

            float[] direction = new float[] { 2, 2, 2, 0 };
            float[] diffuse = new float[] { 0.5f, 0.5f, 0.5f, 1 };
            float[] ambient = new float[] { 0.5f, 0.5f, 0.5f, 1 };
            float[] specular = new float[] { 0.5f, 0.5f, 0.5f, 1 };

            // glLight sets the values of individual light source parameters.
            Gl.glLightfv(light, Gl.GL_POSITION, direction);
            Gl.glLightfv(light, Gl.GL_DIFFUSE, diffuse);
            Gl.glLightfv(light, Gl.GL_AMBIENT, ambient);
            Gl.glLightfv(light, Gl.GL_SPECULAR, specular);

            // GL_LIGHTING - If enabled and no vertex shader is active, use the current lighting parameters to compute the vertex color or index.
            Gl.glEnable(Gl.GL_LIGHTING);
            Gl.glEnable(light);
        }

        private void SetupCamera()
        {
            float speedRotation = 0.5f;

            Gl.glLoadIdentity();
            
            angleRotation = (angleRotation + speedRotation) % 360;
            // gluLookAt creates a viewing matrix derived from an eye point, a reference point indicating the center of the scene, and an UP vector.
            Glu.gluLookAt(12, 12, 6, 0, 0, 0, 0, 0, 1);
            Gl.glRotatef(angleRotation, 0, 0, 1);
        }

        private void DrawSurface()
        {
            int displayList = 0;
            int columns = 50;
            int rows = 50;

            float xMin = -10;
            float xMax = 10;
            float yMin = -10;
            float yMax = 10;

            float scale = 0.8f;

            float zNear = 1f;
            float zFar = 40;
            float fieldView = 60;
            float aspect = (float)openGlControl.Width / (float)openGlControl.Height;
            FunctionSurface function = new FunctionSurface();

            Gl.glClearColor(0, 0, 0, 0);
            Gl.glClear(Gl.GL_COLOR_BUFFER_BIT | Gl.GL_DEPTH_BUFFER_BIT);

            SetupLight();
            SetupMaterial();
            SetupCamera();

            switch (comboBox.SelectedIndex)
            {
                case 0:
                    Gl.glPolygonMode(Gl.GL_FRONT, Gl.GL_POINT);
                    break;
                case 1:
                    Gl.glPolygonMode(Gl.GL_FRONT, Gl.GL_LINE);
                    break;
                case 2:
                    Gl.glPolygonMode(Gl.GL_FRONT, Gl.GL_FILL);
                    break;
            }

            Gl.glMatrixMode(Gl.GL_PROJECTION);
            Gl.glLoadIdentity();

            Glu.gluPerspective(fieldView, aspect, zNear, zFar);
            Gl.glMatrixMode(Gl.GL_MODELVIEW);

            Gl.glPushMatrix();
            // glScale — multiply the current matrix by a general scaling matrix.
            Gl.glScalef(scale, scale, scale);

            if (displayList == 0)
            {
                // glGenLists — generate a contiguous set of empty display lists.
                displayList = Gl.glGenLists(1);
                // glNewList — create or replace a display list.
                Gl.glNewList(displayList, Gl.GL_COMPILE); // Commands are merely compiled.

                float dx = (xMax - xMin) / (columns - 1);
                float dy = (yMax - yMin) / (rows - 1);

                float y = yMin;

                for (int row = 0; row < rows - 1; ++row, y += dy)
                {
                    Gl.glBegin(Gl.GL_TRIANGLE_STRIP);
                    float x = xMin;

                    for (int column = 0; column <= columns; ++column, x += dx)
                    {
                        Vertex v0 = function.CalculateVertex(x, y + dy);
                        Vertex v1 = function.CalculateVertex(x, y);

                        Gl.glNormal3f(v0.nx, v0.ny, v0.nz);
                        Gl.glVertex3f(v0.x, v0.y, v0.z);

                        Gl.glNormal3f(v1.nx, v1.ny, v1.nz);
                        Gl.glVertex3f(v1.x, v1.y, v1.z);
                    }
                    Gl.glEnd();
                }
                Gl.glEndList();
            }
            Gl.glCallList(displayList);
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            Glut.glutInit();
            Glut.glutInitDisplayMode(Glut.GLUT_RGB | Glut.GLUT_DOUBLE | Glut.GLUT_DEPTH);

            // glEnable and glDisable enable and disable various capabilities.
            // glCullFace specifies whether front- or back-facing facets are culled (as specified by mode) when facet culling is enabled.
            // glFrontFace — define front- and back-facing polygons.
            Gl.glEnable(Gl.GL_CULL_FACE); // If enabled, cull polygons based on their winding in window coordinates.
            Gl.glCullFace(Gl.GL_BACK); // Specifies whether front- or back-facing facets are candidates for culling.
            Gl.glFrontFace(Gl.GL_CCW); // Passing GL_CCW to mode selects counterclockwise polygons as front-facing.

            renderTimer.Start();
        }

        private void renderTimer_Tick(object sender, EventArgs e)
        {
            DrawSurface();
            openGlControl.Invalidate();
        }
    }
}