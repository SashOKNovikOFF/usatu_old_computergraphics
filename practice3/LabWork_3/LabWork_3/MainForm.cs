﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Tao.OpenGl;
using Tao.FreeGlut;
using Tao.Platform.Windows;

namespace LabWork_3
{
    public partial class MainForm : Form
    {
        private float rot_1 = 0, rot_2 = 0; // переменные, отвечающие за поворот фигуры

        private const int Iter = 64; // количество точек в каждой медиане объекта
        private double Angle = 2 * Math.PI / Iter; // угол, на который поворачивается линия геометрии фигуры

        private double[,] GeometricArray = new double[Iter, 3]; // массив координат линии геометрии фигуры
        private double[, ,] ResultGeometric = new double[Iter, Iter, 3]; // масссив координат всей фигуры

        private int countElements = 0; // переменная, хранящая количество точек, по которым строится линия геометрии фигуры

        public static int speedOffset = 1; // изначальная скорость вращения фигуры
        public static int viewOffset = -25; // изначальные координаты фигуры по оси OZ
        public static float sizePoint = 5.0f; // размер точек на экране
        public static bool mouseFlag = false, clickFlag = true; // переменные флаги для обработчиков событий мыши

        public MainForm()
        {
            InitializeComponent();
            AnT.InitializeContexts(); // инициализация компонента AnT
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            // инициализация Glut
            Glut.glutInit();
            Glut.glutInitDisplayMode(Glut.GLUT_RGB | Glut.GLUT_DOUBLE | Glut.GLUT_DEPTH);

            // очистка экране
            Gl.glClearColor(255, 255, 255, 1);

            // настройка проекции
            Gl.glViewport(0, 0, AnT.Width, AnT.Height);
            Gl.glMatrixMode(Gl.GL_PROJECTION);
            Gl.glLoadIdentity();
            Glu.gluPerspective(45, (float)AnT.Width / (float)AnT.Height, 0.1, 200);
            Gl.glMatrixMode(Gl.GL_MODELVIEW);
            Gl.glLoadIdentity();

            // настройка параметров освещения
            /* glEnable — enable or disable server-side GL capabilities */
            /* If enabled, do depth comparisons and update the depth buffer. Note that even if the depth buffer exists and the depth mask is non-zero, the depth buffer is not updated if the depth test is disabled. See glDepthFunc and glDepthRange. */
            /* If enabled and no vertex shader is active, use the current lighting parameters to compute the vertex color or index. Otherwise, simply associate the current color or index with each vertex. See glMaterial, glLightModel, and glLight. */
            /* glLight — set light source parameters;  The initial value for GL_LIGHT0 is (1, 1, 1, 1) */
            Gl.glEnable(Gl.GL_DEPTH_TEST);
            Gl.glEnable(Gl.GL_LIGHTING);
            Gl.glEnable(Gl.GL_LIGHT0);

            Compute(); // вычисление координат объекта

            renderTimer.Start(); // запуск таймера
        }

        // функция вычисления координат тела вращения
        private void Compute()
        {
            countElements = 21; // задаём количество точек, по которым строится линия геометрии фигуры

            // задаём координаты внешней части рюмки

            GeometricArray[0, 0] = 0.0;
            GeometricArray[0, 1] = 0;
            GeometricArray[0, 2] = 0.0;

            GeometricArray[1, 0] = 6.3;
            GeometricArray[1, 1] = 0;
            GeometricArray[1, 2] = 0.9;

            GeometricArray[2, 0] = 3.2;
            GeometricArray[2, 1] = 0;
            GeometricArray[2, 2] = 2.4;

            GeometricArray[3, 0] = 0.9;
            GeometricArray[3, 1] = 0;
            GeometricArray[3, 2] = 5.5;

            GeometricArray[4, 0] = 0.5;
            GeometricArray[4, 1] = 0;
            GeometricArray[4, 2] = 8.8;

            GeometricArray[5, 0] = 0.5;
            GeometricArray[5, 1] = 0;
            GeometricArray[5, 2] = 11.0;

            GeometricArray[6, 0] = 0.7;
            GeometricArray[6, 1] = 0;
            GeometricArray[6, 2] = 12.5;

            GeometricArray[7, 0] = 1.3;
            GeometricArray[7, 1] = 0;
            GeometricArray[7, 2] = 16.2;

            GeometricArray[8, 0] = 3.2;
            GeometricArray[8, 1] = 0;
            GeometricArray[8, 2] = 20.9;

            GeometricArray[9, 0] = 5.7;
            GeometricArray[9, 1] = 0;
            GeometricArray[9, 2] = 24.5;

            GeometricArray[10, 0] = 6.9;
            GeometricArray[10, 1] = 0;
            GeometricArray[10, 2] = 27.1;

            GeometricArray[11, 0] = 7.0;
            GeometricArray[11, 1] = 0;
            GeometricArray[11, 2] = 28.6;

            GeometricArray[12, 0] = 6.8;
            GeometricArray[12, 1] = 0;
            GeometricArray[12, 2] = 32.1;

            GeometricArray[13, 0] = 5.9;
            GeometricArray[13, 1] = 0;
            GeometricArray[13, 2] = 36.4;

            // задаём координаты внутренней части рюмки

            GeometricArray[14, 0] = 5.7;
            GeometricArray[14, 1] = 0;
            GeometricArray[14, 2] = 36.2;

            GeometricArray[15, 0] = 6.6;
            GeometricArray[15, 1] = 0;
            GeometricArray[15, 2] = 32.1;

            GeometricArray[16, 0] = 6.7;
            GeometricArray[16, 1] = 0;
            GeometricArray[16, 2] = 28.4;

            GeometricArray[17, 0] = 5.8;
            GeometricArray[17, 1] = 0;
            GeometricArray[17, 2] = 26.4;

            GeometricArray[18, 0] = 3.6;
            GeometricArray[18, 1] = 0;
            GeometricArray[18, 2] = 23.7;

            GeometricArray[19, 0] = 1.3;
            GeometricArray[19, 1] = 0;
            GeometricArray[19, 2] = 22.6;

            GeometricArray[20, 0] = 0.0;
            GeometricArray[20, 1] = 0;
            GeometricArray[20, 2] = 22.6;

            // изначально отрисовываем фигуру в режиме GL_POINTS
            comboBox.SelectedIndex = 0;

            // вычисляем координаты каждой точки тела вращения
            for (int ax = 0; ax < countElements; ax++)
            {
                for (int bx = 0; bx < Iter; bx++)
                {
                    if (bx > 0) // построение очередной медианы тела вращения
                    {
                        ResultGeometric[ax, bx, 0] = ResultGeometric[ax, bx - 1, 0] * Math.Cos(Angle) - ResultGeometric[ax, bx - 1, 1] * Math.Sin(Angle);
                        ResultGeometric[ax, bx, 1] = ResultGeometric[ax, bx - 1, 0] * Math.Sin(Angle) + ResultGeometric[ax, bx - 1, 1] * Math.Cos(Angle);
                        ResultGeometric[ax, bx, 2] = GeometricArray[ax, 2];
                    }
                    else // построение первой медианы тела вращения
                    {
                        ResultGeometric[ax, bx, 0] = GeometricArray[ax, 0] * Math.Cos(Angle) - GeometricArray[ax, 1] * Math.Sin(Angle); /* Можно подставить значение Angle вместо 0 градусов */
                        ResultGeometric[ax, bx, 1] = GeometricArray[ax, 0] * Math.Sin(Angle) + GeometricArray[ax, 1] * Math.Cos(Angle);
                        ResultGeometric[ax, bx, 2] = GeometricArray[ax, 2];
                    }
                }
            }
        }

        // функция отрисовки фигуры
        private void Draw()
        {
            // очищаем буферы, экран и матрицу
            Gl.glClear(Gl.GL_COLOR_BUFFER_BIT | Gl.GL_DEPTH_BUFFER_BIT);
            Gl.glClearColor(255, 255, 255, 1);
            Gl.glLoadIdentity();

            // перемещаем фигуру по оси OZ
            Gl.glTranslated(0, 0, viewOffset - viewBar.Value);

            // пересчитать переменные, отвечающие за скорость и поворот тела
            // если мышка внутри окна приложения
            if (mouseFlag)
                if (clickFlag) // если была нажата клавиша мыши внутри окна программы
                {
                    rot_1 += speedOffset + speedBar.Value;
                    rot_2 += speedOffset + speedBar.Value;
                }
                else
                {
                    rot_1 -= speedOffset + speedBar.Value;
                    rot_2 -= speedOffset + speedBar.Value;
                }

            // поворачиваем фигуру
            /* glRotate produces a rotation of angle degrees around the vector x y z . */
            Gl.glRotated(rot_1, 1, 0, 0);
            Gl.glRotated(rot_2, 0, 1, 0);

            /* glPointSize specifies the rasterized diameter of both aliased and antialiased points. */
            Gl.glPointSize(sizePoint); // устанавливаем размер точек, по которым будет строиться фигура

            switch (comboBox.SelectedIndex)
            {
                case 0: // строим фигуру по точкам
                    {
                        Gl.glBegin(Gl.GL_POINTS);
                        for (int ax = 0; ax < countElements; ax++)
                            for (int bx = 0; bx < Iter; bx++)
                                Gl.glVertex3d(ResultGeometric[ax, bx, 0], ResultGeometric[ax, bx, 1], ResultGeometric[ax, bx, 2]);
                        Gl.glEnd();
                    }
                    break;
                case 1: // строим фигуру по ломаным линиям
                    {
                        /* Draws a connected group of line segments from the first vertex to the last. Vertices n and n + 1 define line n. N - 1 lines are drawn. */
                        Gl.glBegin(Gl.GL_LINE_STRIP);
                        for (int ax = 0; ax < countElements; ax++)
                            for (int bx = 0; bx < Iter; bx++)
                            {
                                Gl.glVertex3d(ResultGeometric[ax, bx, 0], ResultGeometric[ax, bx, 1], ResultGeometric[ax, bx, 2]);
                                Gl.glVertex3d(ResultGeometric[ax + 1, bx, 0], ResultGeometric[ax + 1, bx, 1], ResultGeometric[ax + 1, bx, 2]);
                                if (bx + 1 < Iter - 1)
                                    Gl.glVertex3d(ResultGeometric[ax + 1, bx + 1, 0], ResultGeometric[ax + 1, bx + 1, 1], ResultGeometric[ax + 1, bx + 1, 2]);
                                else
                                    Gl.glVertex3d(ResultGeometric[ax + 1, 0, 0], ResultGeometric[ax + 1, 0, 1], ResultGeometric[ax + 1, 0, 2]);
                            }
                        Gl.glEnd();
                    }
                    break;
                case 2: // строим фигуру по полигонам
                    {
                        /* Treats each group of four vertices as an independent quadrilateral. Vertices 4 ⁢ n - 3 , 4 ⁢ n - 2 , 4 ⁢ n - 1 , and 4 ⁢ n define quadrilateral n. N 4 quadrilaterals are drawn. */
                        Gl.glBegin(Gl.GL_QUADS);
                        for (int ax = 0; ax < countElements; ax++)
                            for (int bx = 0; bx < Iter; bx++)
                            {
                                double x1, x2, x3, x4;
                                double y1, y2, y3, y4;
                                double z1, z2, z3, z4;
                                x1 = x2 = x3 = x4 = 0;
                                y1 = y2 = y3 = y4 = 0;
                                z1 = z2 = z3 = z4 = 0;

                                x1 = ResultGeometric[ax, bx, 0];
                                y1 = ResultGeometric[ax, bx, 1];
                                z1 = ResultGeometric[ax, bx, 2];

                                // если элемент образующей не последний
                                if (ax + 1 < countElements)
                                {
                                    x2 = ResultGeometric[ax + 1, bx, 0];
                                    y2 = ResultGeometric[ax + 1, bx, 1];
                                    z2 = ResultGeometric[ax + 1, bx, 2];

                                    // если элемент окружности текущей точки образующей не последний
                                    if (bx + 1 < Iter)
                                    {
                                        x3 = ResultGeometric[ax + 1, bx + 1, 0];
                                        y3 = ResultGeometric[ax + 1, bx + 1, 1];
                                        z3 = ResultGeometric[ax + 1, bx + 1, 2];

                                        x4 = ResultGeometric[ax, bx + 1, 0];
                                        y4 = ResultGeometric[ax, bx + 1, 1];
                                        z4 = ResultGeometric[ax, bx + 1, 2];
                                    }
                                    else
                                    {
                                        x3 = ResultGeometric[ax + 1, 0, 0];
                                        y3 = ResultGeometric[ax + 1, 0, 1];
                                        z3 = ResultGeometric[ax + 1, 0, 2];

                                        x4 = ResultGeometric[ax, 0, 0];
                                        y4 = ResultGeometric[ax, 0, 1];
                                        z4 = ResultGeometric[ax, 0, 2];
                                    }
                                }
                                else
                                {
                                    x2 = ResultGeometric[0, bx, 0];
                                    y2 = ResultGeometric[0, bx, 1];
                                    z2 = ResultGeometric[0, bx, 2];

                                    // если элемент окружности текущей точки образующей не последний
                                    if (bx + 1 < Iter)
                                    {
                                        x3 = ResultGeometric[0, bx + 1, 0];
                                        y3 = ResultGeometric[0, bx + 1, 1];
                                        z3 = ResultGeometric[0, bx + 1, 2];

                                        x4 = ResultGeometric[ax, bx + 1, 0];
                                        y4 = ResultGeometric[ax, bx + 1, 1];
                                        z4 = ResultGeometric[ax, bx + 1, 2];
                                    }
                                    else
                                    {
                                        x3 = ResultGeometric[0, 0, 0];
                                        y3 = ResultGeometric[0, 0, 1];
                                        z3 = ResultGeometric[0, 0, 2];

                                        x4 = ResultGeometric[ax, 0, 0];
                                        y4 = ResultGeometric[ax, 0, 1];
                                        z4 = ResultGeometric[ax, 0, 2];
                                    }
                                }

                                // вычисляем нормали к каждой грани объекта
                                double n1, n2, n3;
                                n1 = n2 = n3 = 0;

                                if (ax == 0)
                                {
                                    n1 = (y2 - y1) * (z1 - z3) - (y3 - y1) * (z2 - z1);
                                    n2 = (z2 - z1) * (x3 - x1) - (z3 - z1) * (x2 - x1);
                                    n3 = (x2 - x1) * (y3 - y1) - (x3 - x1) * (y2 - y1);
                                }
                                else
                                {
                                    n1 = (y4 - y3) * (z1 - z3) - (y1 - y3) * (z4 - z3);
                                    n2 = (z4 - z3) * (x1 - x3) - (z1 - z3) * (x4 - x3);
                                    n3 = (x4 - x3) * (y1 - y3) - (x1 - x3) * (y4 - y3);
                                }

                                // нормализуем полученный вектора
                                double n5 = (double)Math.Sqrt(n1 * n1 + n2 * n2 + n3 * n3);
                                n1 /= (n5 + 0.01);
                                n2 /= (n5 + 0.01);
                                n3 /= (n5 + 0.01);

                                /* glNormal — set the current normal vector */
                                Gl.glNormal3d(-n1, -n2, -n3); // устанавливаем текущий вектор нормали
                                
                                // строим полигон
                                Gl.glVertex3d(x1, y1, z1);
                                Gl.glVertex3d(x2, y2, z2);
                                Gl.glVertex3d(x3, y3, z3);
                                Gl.glVertex3d(x4, y4, z4);
                            }
                    }
                    Gl.glEnd();
                    break;
            }

            Gl.glPopMatrix();

            Gl.glFlush();
            AnT.Invalidate();
        }

        // обработчик функции, запускающий таймер
        private void renderTimer_Tick(object sender, EventArgs e)
        {
            Draw(); // отрисовываем тело вращения
        }

        // обработчик функции, отвечающий за появление мыши в окне приложения
        private void AnT_MouseEnter(object sender, EventArgs e)
        {
            mouseFlag = true; // переменная-флаг указывает на то, что мышка находится в окне приложения
        }

        // обработчик функции, отвечающий за выход мыши из окна приложения
        private void AnT_MouseLeave(object sender, EventArgs e)
        {
            mouseFlag = false;  // переменная-флаг указывает на то, что мышка находится вне окна приложения
        }

        // обработчик функции, отвечающий за нажатие кнопки мыши в окне приложения
        private void AnT_Click(object sender, EventArgs e)
        {
            clickFlag = !clickFlag; // отмечаем нажатие кнопки мыши на экране
        }
    }
}
