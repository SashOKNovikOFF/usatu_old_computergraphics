﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Tao.OpenGl;
using Tao.FreeGlut;
using Tao.Platform.Windows;

namespace ExampleRotation
{
    public partial class MainForm : Form
    {
        private float rot_1, rot_2;

        private double[,] GeometricArray = new double[64, 3];
        private double[, ,] ResultGeometric = new double[64, 64, 3];

        private int countElements = 0;

        private double Angle = 2 * Math.PI / 64;
        private int Iter = 64;

        public static int beginOffset = -10;

        public MainForm()
        {
            InitializeComponent();
            AnT.InitializeContexts();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            Glut.glutInit();
            /* Bit mask to select a window with a depth buffer. */
            Glut.glutInitDisplayMode(Glut.GLUT_RGB | Glut.GLUT_DOUBLE | Glut.GLUT_DEPTH);

            Gl.glClearColor(255, 255, 255, 1);

            Gl.glViewport(0, 0, AnT.Width, AnT.Height);

            Gl.glMatrixMode(Gl.GL_PROJECTION);
            Gl.glLoadIdentity();

            /* gluPerspective specifies a viewing frustum into the world coordinate system. */
            /* fovy - Specifies the field of view angle, in degrees, in the y direction.
               aspect - Specifies the aspect ratio that determines the field of view in the x direction. The aspect ratio is the ratio of x (width) to y (height).
               zNear - Specifies the distance from the viewer to the near clipping plane (always positive).
               zFar - Specifies the distance from the viewer to the far clipping plane (always positive). */
            Glu.gluPerspective(45, (float)AnT.Width / (float)AnT.Height, 0.1, 200);
            Gl.glMatrixMode(Gl.GL_MODELVIEW);
            Gl.glLoadIdentity();

            /* glEnable — enable or disable server-side GL capabilities */
            /* If enabled, do depth comparisons and update the depth buffer. Note that even if the depth buffer exists and the depth mask is non-zero, the depth buffer is not updated if the depth test is disabled. See glDepthFunc and glDepthRange. */
            /* If enabled and no vertex shader is active, use the current lighting parameters to compute the vertex color or index. Otherwise, simply associate the current color or index with each vertex. See glMaterial, glLightModel, and glLight. */
            /* glLight — set light source parameters;  The initial value for GL_LIGHT0 is (1, 1, 1, 1) */
            Gl.glEnable(Gl.GL_DEPTH_TEST);
            Gl.glEnable(Gl.GL_LIGHTING);
            Gl.glEnable(Gl.GL_LIGHT0);

            countElements = 8;

            // заоплнение точек в массиве GeometricArray
            GeometricArray[0, 0] = 0;
            GeometricArray[0, 1] = 0;
            GeometricArray[0, 2] = 0;

            GeometricArray[1, 0] = 0.7;
            GeometricArray[1, 1] = 0;
            GeometricArray[1, 2] = 1;

            GeometricArray[2, 0] = 1.3;
            GeometricArray[2, 1] = 0;
            GeometricArray[2, 2] = 2;

            GeometricArray[3, 0] = 1.0;
            GeometricArray[3, 1] = 0;
            GeometricArray[3, 2] = 3;

            GeometricArray[4, 0] = 0.5;
            GeometricArray[4, 1] = 0;
            GeometricArray[4, 2] = 4;

            GeometricArray[5, 0] = 3;
            GeometricArray[5, 1] = 0;
            GeometricArray[5, 2] = 6;

            GeometricArray[6, 0] = 1;
            GeometricArray[6, 1] = 0;
            GeometricArray[6, 2] = 7;

            GeometricArray[7, 0] = 0;
            GeometricArray[7, 1] = 0;
            GeometricArray[7, 2] = 7.2;

            // отрисовываем фигуру в режиме GL_POINTS
            comboBox.SelectedIndex = 0;

            // вычисляем координаты каждой точки тела вращения
            for (int ax = 0; ax < countElements; ax++)
            {
                for (int bx = 0; bx < Iter; bx++)
                {
                    if (bx > 0)
                    {
                        ResultGeometric[ax, bx, 0] = ResultGeometric[ax, bx - 1, 0] * Math.Cos(Angle) - ResultGeometric[ax, bx - 1, 1] * Math.Sin(Angle);
                        ResultGeometric[ax, bx, 1] = ResultGeometric[ax, bx - 1, 0] * Math.Sin(Angle) + ResultGeometric[ax, bx - 1, 1] * Math.Cos(Angle);
                        ResultGeometric[ax, bx, 2] = GeometricArray[ax, 2];
                    }
                    else
                    {
                        ResultGeometric[ax, bx, 0] = GeometricArray[ax, 0] * Math.Cos(Angle) - GeometricArray[ax, 1] * Math.Sin(Angle); /* Можно подставить значение Angle вместо 0 градусов */
                        ResultGeometric[ax, bx, 1] = GeometricArray[ax, 0] * Math.Sin(Angle) + GeometricArray[ax, 1] * Math.Cos(Angle);
                        ResultGeometric[ax, bx, 2] = GeometricArray[ax, 2];
                    }
                }
            }

            RenderTimer.Start();
        }

        private void Draw()
        {
            rot_1++;
            rot_2++;

            Gl.glClear(Gl.GL_COLOR_BUFFER_BIT | Gl.GL_DEPTH_BUFFER_BIT);
            Gl.glClearColor(255, 255, 255, 1);

            Gl.glLoadIdentity();
            Gl.glTranslated(0, 0, beginOffset - trackBar.Value);
            /* glRotate produces a rotation of angle degrees around the vector x y z . */
            Gl.glRotated(rot_1, 1, 0, 0);
            Gl.glRotated(rot_2, 0, 1, 0);

            /* glPointSize specifies the rasterized diameter of both aliased and antialiased points. */
            /* устанавливаем размер точек, равный 5 */
            Gl.glPointSize(5.0f);

            switch (comboBox.SelectedIndex)
            {
                case 0:
                    {
                        Gl.glBegin(Gl.GL_POINTS);
                        for (int ax = 0; ax < countElements; ax++)
                            for (int bx = 0; bx < Iter; bx++)
                                Gl.glVertex3d(ResultGeometric[ax, bx, 0], ResultGeometric[ax, bx, 1], ResultGeometric[ax, bx, 2]);
                        Gl.glEnd();
                    }
                    break;
                case 1:
                    {
                        /* Draws a connected group of line segments from the first vertex to the last. Vertices n and n + 1 define line n. N - 1 lines are drawn. */
                        Gl.glBegin(Gl.GL_LINE_STRIP);
                        for (int ax = 0; ax < countElements; ax++)
                            for (int bx = 0; bx < Iter; bx++)
                            {
                                Gl.glVertex3d(ResultGeometric[ax, bx, 0], ResultGeometric[ax, bx, 1], ResultGeometric[ax, bx, 2]);
                                Gl.glVertex3d(ResultGeometric[ax + 1, bx, 0], ResultGeometric[ax + 1, bx, 1], ResultGeometric[ax + 1, bx, 2]);
                                if (bx + 1 < Iter - 1)
                                    Gl.glVertex3d(ResultGeometric[ax + 1, bx + 1, 0], ResultGeometric[ax + 1, bx + 1, 1], ResultGeometric[ax + 1, bx + 1, 2]);
                                else
                                    Gl.glVertex3d(ResultGeometric[ax + 1, 0, 0], ResultGeometric[ax + 1, 0, 1], ResultGeometric[ax + 1, 0, 2]);
                            }
                        Gl.glEnd();
                    }
                    break;
                case 2:
                    {
                        /* Treats each group of four vertices as an independent quadrilateral. Vertices 4 ⁢ n - 3 , 4 ⁢ n - 2 , 4 ⁢ n - 1 , and 4 ⁢ n define quadrilateral n. N 4 quadrilaterals are drawn. */
                        Gl.glBegin(Gl.GL_QUADS);
                        for (int ax = 0; ax < countElements; ax++)
                            for (int bx = 0; bx < Iter; bx++)
                            {
                                double x1, x2, x3, x4;
                                double y1, y2, y3, y4;
                                double z1, z2, z3, z4;
                                x1 = x2 = x3 = x4 = 0;
                                y1 = y2 = y3 = y4 = 0;
                                z1 = z2 = z3 = z4 = 0;

                                x1 = ResultGeometric[ax, bx, 0];
                                y1 = ResultGeometric[ax, bx, 1];
                                z1 = ResultGeometric[ax, bx, 2];

                                // если элемент образующей не последний
                                if (ax + 1 < countElements)
                                {
                                    x2 = ResultGeometric[ax + 1, bx, 0];
                                    y2 = ResultGeometric[ax + 1, bx, 1];
                                    z2 = ResultGeometric[ax + 1, bx, 2];

                                    // если элемент окружности текущей точки образующей не последний
                                    if (bx + 1 < Iter - 1)
                                    {
                                        x3 = ResultGeometric[ax + 1, bx + 1, 0];
                                        y3 = ResultGeometric[ax + 1, bx + 1, 1];
                                        z3 = ResultGeometric[ax + 1, bx + 1, 2];

                                        x4 = ResultGeometric[ax, bx + 1, 0];
                                        y4 = ResultGeometric[ax, bx + 1, 1];
                                        z4 = ResultGeometric[ax, bx + 1, 2];
                                    }
                                    else
                                    {
                                        x3 = ResultGeometric[ax + 1, 0, 0];
                                        y3 = ResultGeometric[ax + 1, 0, 1];
                                        z3 = ResultGeometric[ax + 1, 0, 2];

                                        x4 = ResultGeometric[ax, 0, 0];
                                        y4 = ResultGeometric[ax, 0, 1];
                                        z4 = ResultGeometric[ax, 0, 2];
                                    }
                                }
                                else
                                {
                                    x2 = ResultGeometric[0, bx, 0];
                                    y2 = ResultGeometric[0, bx, 1];
                                    z2 = ResultGeometric[0, bx, 2];

                                    if (bx + 1 < Iter - 1)
                                    {
                                        x3 = ResultGeometric[0, bx + 1, 0];
                                        y3 = ResultGeometric[0, bx + 1, 1];
                                        z3 = ResultGeometric[0, bx + 1, 2];

                                        x4 = ResultGeometric[ax, bx + 1, 0];
                                        y4 = ResultGeometric[ax, bx + 1, 1];
                                        z4 = ResultGeometric[ax, bx + 1, 2];
                                    }
                                    else
                                    {
                                        x3 = ResultGeometric[0, 0, 0];
                                        y3 = ResultGeometric[0, 0, 1];
                                        z3 = ResultGeometric[0, 0, 2];

                                        x4 = ResultGeometric[ax, 0, 0];
                                        y4 = ResultGeometric[ax, 0, 1];
                                        z4 = ResultGeometric[ax, 0, 2];
                                    }
                                }

                                double n1, n2, n3;
                                n1 = n2 = n3 = 0;

                                if (ax == 0)
                                {
                                    n1 = (y2 - y1) * (z1 - z3) - (y3 - y1) * (z2 - z1);
                                    n2 = (z2 - z1) * (x3 - x1) - (z3 - z1) * (x2 - x1);
                                    n3 = (x2 - x1) * (y3 - y1) - (x3 - x1) * (y2 - y1);
                                }
                                else
                                {
                                    n1 = (y4 - y3) * (z1 - z3) - (y1 - y3) * (z4 - z3);
                                    n2 = (z4 - z3) * (x1 - x3) - (z1 - z3) * (x4 - x3);
                                    n3 = (x4 - x3) * (y1 - y3) - (x1 - x3) * (y4 - y3);
                                }

                                double n5 = (double)Math.Sqrt(n1 * n1 + n2 * n2 + n3 * n3);
                                n1 /= (n5 + 0.01);
                                n2 /= (n5 + 0.01);
                                n3 /= (n5 + 0.01);

                                /* glNormal — set the current normal vector */
                                Gl.glNormal3d(-n1, -n2, -n3);

                                Gl.glVertex3d(x1, y1, z1);
                                Gl.glVertex3d(x2, y2, z2);
                                Gl.glVertex3d(x3, y3, z3);
                                Gl.glVertex3d(x4, y4, z4);
                            }
                    }
                    Gl.glEnd();
                    break;
            }

            Gl.glPopMatrix();

            Gl.glFlush();
            AnT.Invalidate();
        }

        private void RenderTimer_Tick(object sender, EventArgs e)
        {
            Draw();
        }
    }
}
