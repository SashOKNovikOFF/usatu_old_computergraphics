﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Tao.DevIl;
using Tao.OpenGl;
using Tao.FreeGlut;
using Tao.Platform.Windows;

// 1. Что такое Gl.GL_RGB?

namespace ExampleTexture
{
    public partial class MainForm : Form
    {
        int rotation = 0;
        bool isTextureLoad = false;

        float mouseX = 0, mouseY = 0;
        float mouseLastX = 0, mouseLastY = 0;
        bool mouseButton = false;
        float rotationX, rotationY;

        public string textureName = "";
        public int imageID = 0;
        public uint glTextureObject = 0;        

        public MainForm()
        {
            InitializeComponent();
            openGlControl.InitializeContexts();
        }

        private void exitToolStrip_Click(object sender, EventArgs e)
        {
            DialogResult exitMSGBox = MessageBox.Show("Вы действительно хотите выйти?", "Внимание!", MessageBoxButtons.YesNo, MessageBoxIcon.Hand);

            if (exitMSGBox == DialogResult.Yes)
                Application.Exit();
        }

        private void openFileToolStrip_Click(object sender, EventArgs e)
        {
            DialogResult result = openFileDialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                Il.ilGenImages(1, out imageID); //ilGenImages stores Num image names in Images. The image names stored in Images can be used with ilBindImage after calling ilGenImages.
                Il.ilBindImage(imageID); // ilBindImage binds the current image to the image described by the image name in Image.

                string url = openFileDialog.FileName;

                if (Il.ilLoadImage(url)) // ilLoadImage is the main DevIL loading function. All you do is pass ilLoadImage the filename of the image you wish to load. 
                {
                    // ilGetInteger returns the value of a selected mode.
                    int width = Il.ilGetInteger(Il.IL_IMAGE_WIDTH);
                    int height = Il.ilGetInteger(Il.IL_IMAGE_HEIGHT);

                    int bitsPerPixel = Il.ilGetInteger(Il.IL_IMAGE_BITS_PER_PIXEL);
                    switch (bitsPerPixel)
                    {
                        case 24:
                            // Il.ilGetData() - gets image data to allow direct access and modification to the contents of the image.
                            glTextureObject = MakeGlTexture(Gl.GL_RGB, Il.ilGetData(), width, height);
                            break;
                        case 32:
                            glTextureObject = MakeGlTexture(Gl.GL_RGBA, Il.ilGetData(), width, height);
                            break;
                    }
                    
                    isTextureLoad = true;
                    Il.ilDeleteImages(1, ref imageID); // ilDeleteImages deletes Num image names specified in Image.
                }
            }
        }

        private uint MakeGlTexture(int format, IntPtr pixels, int weight, int height)
        {
            uint texObject;

            Gl.glGenTextures(1, out texObject); // glGenTextures — generate texture names.
            Gl.glPixelStorei(Gl.GL_UNPACK_ALIGNMENT, 1); // glPixelStore — set pixel storage modes; Specifies the alignment requirements for the start of each pixel row in memory (1 - byte-alignment).
            Gl.glBindTexture(Gl.GL_TEXTURE_2D, texObject); // glBindTexture — bind a named texture to a texturing target; Gl.GL_TEXTURE_2D - двухмерная текстура (They have width and height, but no depth).

            Gl.glTexParameteri(Gl.GL_TEXTURE_2D, Gl.GL_TEXTURE_WRAP_S, Gl.GL_REPEAT); // Установка режима заворачивания текстурной координаты (по оси S, с повторением текстуры)
            Gl.glTexParameteri(Gl.GL_TEXTURE_2D, Gl.GL_TEXTURE_WRAP_T, Gl.GL_REPEAT); // Установка режима заворачивания текстурной координаты (по оси T, с повторением текстуры)
            Gl.glTexParameteri(Gl.GL_TEXTURE_2D, Gl.GL_TEXTURE_MAG_FILTER, Gl.GL_LINEAR); // The texture magnification function is used when the pixel being textured maps to an area less than or equal to one texture element; GL_LINEAR - более гладкий переход у границ текстур.
            Gl.glTexParameteri(Gl.GL_TEXTURE_2D, Gl.GL_TEXTURE_MIN_FILTER, Gl.GL_LINEAR); // The texture minifying function is used whenever the pixel being textured maps to an area greater than one texture element; GL_LINEAR - Returns the weighted average of the four texture elements that are closest to the center of the pixel being textured.
            Gl.glTexEnvf(Gl.GL_TEXTURE_ENV, Gl.GL_TEXTURE_ENV_MODE, Gl.GL_REPLACE); // glTexEnv — set texture environment parameters; Gl.GL_REPLACE - "вставляет" текстуру на "нужное" место

            // glTexImage2D — specify a two-dimensional texture image.
            switch (format)
            {
                case Gl.GL_RGB:
                    Gl.glTexImage2D(Gl.GL_TEXTURE_2D, 0, Gl.GL_RGB, weight, height, 0, Gl.GL_RGB, Gl.GL_UNSIGNED_BYTE, pixels);
                    break;
                case Gl.GL_RGBA:
                    Gl.glTexImage2D(Gl.GL_TEXTURE_2D, 0, Gl.GL_RGBA, weight, height, 0, Gl.GL_RGBA, Gl.GL_UNSIGNED_BYTE, pixels);
                    break;
            }

            return texObject;
        }

        private void DrawTexture()
        {
            if (isTextureLoad)
            {
                Gl.glDisable(Gl.GL_CULL_FACE);

                rotation++;
                if (rotation > 360)
                    rotation = 0;

                Gl.glClear(Gl.GL_COLOR_BUFFER_BIT | Gl.GL_DEPTH_BUFFER_BIT);
                Gl.glClearColor(255, 255, 255, 1);
                Gl.glLoadIdentity();

                Gl.glEnable(Gl.GL_TEXTURE_2D);
                Gl.glBindTexture(Gl.GL_TEXTURE_2D, glTextureObject);

                Gl.glPushMatrix();

                Gl.glTranslated(0, -1, -3);
                Gl.glRotated(rotation, 0, 1, 0);

                Gl.glBegin(Gl.GL_QUADS);
                    // glTexCoord — set the current texture coordinates.
                    Gl.glTexCoord2f(0, 0);
                    Gl.glVertex3d(0, 0, 0);
                    Gl.glTexCoord2f(0, 1);
                    Gl.glVertex3d(0, 1, 0);
                    Gl.glTexCoord2f(1, 1);
                    Gl.glVertex3d(1, 1, 0);
                    Gl.glTexCoord2f(1, 0);
                    Gl.glVertex3d(1, 0, 0);
                Gl.glEnd();

                Gl.glBegin(Gl.GL_TRIANGLES);
                Gl.glTexCoord2f(1, 0);
                Gl.glVertex3d(1, 1, 0);
                Gl.glTexCoord2f(0.5f, 1);
                Gl.glVertex3d(0.5f, 2, 0);
                Gl.glTexCoord2f(0, 0);
                Gl.glVertex3d(0, 1, 0);
                Gl.glEnd();

                Gl.glPopMatrix();
                Gl.glDisable(Gl.GL_TEXTURE_2D);

                openGlControl.Invalidate();
            }
        }

        private void DrawCube()
        {
            const int faceCount = 6;
            const int verticesCount = 8;
            float scale = 0.5f;

            float[][] vertices;
            vertices = new float[verticesCount][]
            {
                new float[] {-1, -1, -1},
                new float[] {1, -1, -1},
                new float[] {1, 1, -1},
                new float[] {-1, 1, -1},
                new float[] {-1, -1, 1},
                new float[] {1, -1, 1},
                new float[] {1, 1, 1},
                new float[] {-1, 1, 1}
            };

            int[,] faces;
            faces = new int[,]
            {
                {4, 7, 3, 0},
                {5, 1, 2, 6},
                {4, 0, 1, 5},
                {7, 6, 2, 3},
                {0, 3, 2, 1},
                {4, 5, 6, 7}
            };

            byte[][] faceColors;
            faceColors = new byte[faceCount][]
            {
                new byte[] {127, 0, 0, 0},
                new byte[] {0, 127, 0, 0},
                new byte[] {0, 0, 127, 0},
                new byte[] {127, 127, 0, 0},
                new byte[] {0, 127, 127, 0},
                new byte[] {127, 0, 127, 0}
            };

            Gl.glClearColor(255, 255, 255, 0);

            if (!isTextureLoad)
            {
                Gl.glClear(Gl.GL_COLOR_BUFFER_BIT | Gl.GL_DEPTH_BUFFER_BIT);

                Gl.glEnable(Gl.GL_CULL_FACE);
                Gl.glCullFace(Gl.GL_BACK);
                Gl.glFrontFace(Gl.GL_CCW);
                /* If enabled, do depth comparisons and update the depth buffer. Note that even if the depth buffer exists and the depth mask is non-zero, the depth buffer is not updated if the depth test is disabled. */
                Gl.glEnable(Gl.GL_DEPTH_TEST);

                Gl.glPushMatrix();

                Gl.glRotatef(rotationX, 1, 0, 0);
                Gl.glRotatef(rotationY, 0, 1, 0);

                Gl.glScalef(scale, scale, scale);
                Gl.glClearColor(255, 255, 255, 0);

                Gl.glBegin(Gl.GL_QUADS);
                for (int face = 0; face < faceCount; ++face)
                {
                    Gl.glColor4bv(faceColors[face]); // glColor — set the current color
                    for (int i = 0; i < 4; ++i)
                    {
                        int vertexIndex = faces[face, i];
                        Gl.glVertex3fv(vertices[vertexIndex]);
                    }
                }
                Gl.glEnd();

                Gl.glPopMatrix();
            }
            
            Gl.glFlush();
            openGlControl.Invalidate();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            const float zNear = 0.5f;
            const float zFar = 10;
            const float fieldView = 45;
            float aspect = (float)openGlControl.Width / (float)openGlControl.Height;

            Glut.glutInit();
            Glut.glutInitDisplayMode(Glut.GLUT_RGB | Glut.GLUT_DOUBLE);

            /* You need to initialize each library (IL, ILU, and ILUT) separately. */
            Il.ilInit(); // IL Initialization
            /* All images have a certain set of characteristics: origin of the image, format of the image, type of the image, and more.*/
            Il.ilEnable(Il.IL_ORIGIN_SET); // If enabled, the origin is specified at an absolute position, and all images loaded or saved adhere to this set origin.

            Gl.glClearColor(255, 255, 255, 1);
            Gl.glClear(Gl.GL_COLOR_BUFFER_BIT | Gl.GL_DEPTH_BUFFER_BIT);

            Gl.glMatrixMode(Gl.GL_PROJECTION);
            Gl.glLoadIdentity();
            Glu.gluPerspective(fieldView, aspect, zNear, zFar);

            Gl.glMatrixMode(Gl.GL_MODELVIEW);
            Gl.glLoadIdentity();

            Glu.gluLookAt(0, 0, 3, 0, 0, 0, 0, 1, 0);
            
            renderTimer.Start();
        }

        private void openGlControl_MouseMove(object sender, MouseEventArgs e)
        {
            float dx = e.X - mouseX;
            float dy = e.Y - mouseY;

            if (mouseButton)
            {
                rotationX = (dy * 180) / openGlControl.Height + mouseLastX;
                rotationY = (dx * 180) / openGlControl.Width + mouseLastY;

                DrawCube();
                openGlControl.Invalidate();
            }
        }

        private void openGlControl_MouseDown(object sender, MouseEventArgs e)
        {
            mouseButton = true;
            mouseX = e.X;
            mouseY = e.Y;
        }

        private void openGlControl_MouseUp(object sender, MouseEventArgs e)
        {
            mouseButton = false;
            mouseLastX = rotationX;
            mouseLastY = rotationY;
        }

        private void renderTimer_Tick(object sender, EventArgs e)
        {
            if (isTextureLoad)
                DrawTexture();
            else
                DrawCube();
        }
    }
}
