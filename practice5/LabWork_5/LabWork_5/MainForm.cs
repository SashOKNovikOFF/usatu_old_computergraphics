﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Tao.DevIl;
using Tao.OpenGl;
using Tao.FreeGlut;
using Tao.Platform.Windows;

namespace LabWork_5
{
    public partial class MainForm : Form
    {
        int rotation = 0; // переменная, отвечающая за поворот фигуры
        bool isTextureLoad = false; // переменная, отмечающая, была ли загружена текстура

        float mouseX = 0, mouseY = 0; // текущие координаты мыши
        float mouseLastX = 0, mouseLastY = 0; // последние координаты мыши
        bool mouseButton = false; // переменная, отмечающая, была ли нажата кнопка мыши
        float rotationX, rotationY; // переменные, отвечающие за поворот фигуры по оси OX и OY

        public string textureName = ""; // название текстуры
        public int imageID = 0; // номер текстуры
        public uint glTextureObject = 0; // переменная, отвечающая за загруженную текстуру

        public MainForm()
        {
            InitializeComponent();
            openGlControl.InitializeContexts(); // инициализация компонента openGlControl
        }

        // обработчик событий, отвечающий за окончание работы программы по желанию пользователя при нажатии на кнопку "Выход"
        private void exitToolStrip_Click(object sender, EventArgs e)
        {
            // предложить пользователю выйти из программы
            DialogResult exitMSGBox = MessageBox.Show("Вы действительно хотите выйти?", "Внимание!", MessageBoxButtons.YesNo, MessageBoxIcon.Hand);

            // если пользователь выбрал ответ "Да"
            if (exitMSGBox == DialogResult.Yes)
                Application.Exit();
        }
        
        // обработчик событий, отвечающий за загрузку изображения, выбранного пользовальтем при нажатии на кнопку "Октрыть..."
        private void openFileToolStrip_Click(object sender, EventArgs e)
        {
            DialogResult result = openFileDialog.ShowDialog();

            // если изображение было выбрано
            if (result == DialogResult.OK)
            {
                Il.ilGenImages(1, out imageID); //ilGenImages stores Num image names in Images. The image names stored in Images can be used with ilBindImage after calling ilGenImages.
                Il.ilBindImage(imageID); // ilBindImage binds the current image to the image described by the image name in Image.

                string url = openFileDialog.FileName; // путь к изображению в файловой системе

                if (Il.ilLoadImage(url)) // ilLoadImage is the main DevIL loading function. All you do is pass ilLoadImage the filename of the image you wish to load. 
                {
                    // ilGetInteger returns the value of a selected mode.
                    int width = Il.ilGetInteger(Il.IL_IMAGE_WIDTH);
                    int height = Il.ilGetInteger(Il.IL_IMAGE_HEIGHT);

                    int bitsPerPixel = Il.ilGetInteger(Il.IL_IMAGE_BITS_PER_PIXEL); // количество битов на 1 пиксель изображения
                    // создаём текстуру, используя режим GL_RGB или GL_RGBA
                    switch (bitsPerPixel)
                    {
                        case 24:
                            // Il.ilGetData() - gets image data to allow direct access and modification to the contents of the image.
                            glTextureObject = MakeGlTexture(Gl.GL_RGB, Il.ilGetData(), width, height);
                            break;
                        case 32:
                            glTextureObject = MakeGlTexture(Gl.GL_RGBA, Il.ilGetData(), width, height);
                            break;
                    }

                    isTextureLoad = true; // отмечаем, что текстура загружена
                    Il.ilDeleteImages(1, ref imageID); // ilDeleteImages deletes Num image names specified in Image.
                }
            }
        }

        // создание текстуры в памяти OpenGL
        private uint MakeGlTexture(int format, IntPtr pixels, int weight, int height)
        {
            uint texObject; // идентификатор текстурного объекта

            Gl.glGenTextures(1, out texObject); // glGenTextures — generate texture names.
            Gl.glPixelStorei(Gl.GL_UNPACK_ALIGNMENT, 1); // glPixelStore — set pixel storage modes; Specifies the alignment requirements for the start of each pixel row in memory (1 - byte-alignment).
            Gl.glBindTexture(Gl.GL_TEXTURE_2D, texObject); // glBindTexture — bind a named texture to a texturing target; Gl.GL_TEXTURE_2D - двухмерная текстура (They have width and height, but no depth).

            Gl.glTexParameteri(Gl.GL_TEXTURE_2D, Gl.GL_TEXTURE_WRAP_S, Gl.GL_REPEAT); // Установка режима заворачивания текстурной координаты (по оси S, с повторением текстуры)
            Gl.glTexParameteri(Gl.GL_TEXTURE_2D, Gl.GL_TEXTURE_WRAP_T, Gl.GL_REPEAT); // Установка режима заворачивания текстурной координаты (по оси T, с повторением текстуры)
            Gl.glTexParameteri(Gl.GL_TEXTURE_2D, Gl.GL_TEXTURE_MAG_FILTER, Gl.GL_LINEAR); // The texture magnification function is used when the pixel being textured maps to an area less than or equal to one texture element; GL_LINEAR - более гладкий переход у границ текстур.
            Gl.glTexParameteri(Gl.GL_TEXTURE_2D, Gl.GL_TEXTURE_MIN_FILTER, Gl.GL_LINEAR); // The texture minifying function is used whenever the pixel being textured maps to an area greater than one texture element; GL_LINEAR - Returns the weighted average of the four texture elements that are closest to the center of the pixel being textured.
            Gl.glTexEnvf(Gl.GL_TEXTURE_ENV, Gl.GL_TEXTURE_ENV_MODE, Gl.GL_REPLACE); // glTexEnv — set texture environment parameters; Gl.GL_REPLACE - "вставляет" текстуру на "нужное" место

            // glTexImage2D — specify a two-dimensional texture image.
            switch (format)
            {
                case Gl.GL_RGB:
                    Gl.glTexImage2D(Gl.GL_TEXTURE_2D, 0, Gl.GL_RGB, weight, height, 0, Gl.GL_RGB, Gl.GL_UNSIGNED_BYTE, pixels);
                    break;
                case Gl.GL_RGBA:
                    Gl.glTexImage2D(Gl.GL_TEXTURE_2D, 0, Gl.GL_RGBA, weight, height, 0, Gl.GL_RGBA, Gl.GL_UNSIGNED_BYTE, pixels);
                    break;
            }

            return texObject;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            // настраиваем режим проекции
            const float zNear = 0.5f;
            const float zFar = 10;
            const float fieldView = 45;
            float aspect = (float)openGlControl.Width / (float)openGlControl.Height;

            // инициализируем библиотеку Glut
            Glut.glutInit();
            Glut.glutInitDisplayMode(Glut.GLUT_RGB | Glut.GLUT_DOUBLE);

            /* You need to initialize each library (IL, ILU, and ILUT) separately. */
            Il.ilInit(); // IL Initialization
            /* All images have a certain set of characteristics: origin of the image, format of the image, type of the image, and more.*/
            Il.ilEnable(Il.IL_ORIGIN_SET); // If enabled, the origin is specified at an absolute position, and all images loaded or saved adhere to this set origin.

            // очищаем экран, буферы цвета и глубины
            Gl.glClearColor(255, 255, 255, 1);
            Gl.glClear(Gl.GL_COLOR_BUFFER_BIT | Gl.GL_DEPTH_BUFFER_BIT);

            // устанавливаем режим проекции
            Gl.glMatrixMode(Gl.GL_PROJECTION);
            Gl.glLoadIdentity();
            Glu.gluPerspective(fieldView, aspect, zNear, zFar);

            // устанавливаем объектно-видовую матрицу и очищаем её
            Gl.glMatrixMode(Gl.GL_MODELVIEW);
            Gl.glLoadIdentity();

            Glu.gluLookAt(0, 0, 3, 0, 0, 0, 0, 1, 0); 

            renderTimer.Start(); // запускаем таймер
        }

        // обработчик событий, отвечающий за движение мыши в окне приложения
        private void openGlControl_MouseMove(object sender, MouseEventArgs e)
        {
            // определяем перемещение мыши в окне программы
            float dx = e.X - mouseX;
            float dy = e.Y - mouseY;

            // если клавиша была нажата
            if (mouseButton)
            {
                // пересчитать переменные, отвечающие за поворот фигуры
                rotationX = (dy * 180) / openGlControl.Height + mouseLastX;
                rotationY = (dx * 180) / openGlControl.Width + mouseLastY;

                // перерисовать фигуру заново
                DrawTruncatedDodecahedron();
                openGlControl.Invalidate();
            }
        }

        // обработчик событий, отвечающий за нажатие кнопки мыши в окне приложения
        private void openGlControl_MouseDown(object sender, MouseEventArgs e)
        {
            // отметить, что клавиша была нажата и зафиксировать координаты мыши при нажатии
            mouseButton = true;
            mouseX = e.X;
            mouseY = e.Y;
        }

        // обработчик событий, отвечающий за конец нажатия мыши в окне приложения
        private void openGlControl_MouseUp(object sender, MouseEventArgs e)
        {
            // ометить, что клавиша была отжата и зафиксировать последнее положение мыши в окне приложения
            mouseButton = false;
            mouseLastX = rotationX;
            mouseLastY = rotationY;
        }

        // запуск таймера
        private void renderTimer_Tick(object sender, EventArgs e)
        {
            DrawTruncatedDodecahedron(); // отрисовка фигуры
        }
    }
}
