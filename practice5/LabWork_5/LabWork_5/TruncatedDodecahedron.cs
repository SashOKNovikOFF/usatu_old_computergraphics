﻿using System;
using System.Windows.Forms;

using Tao.DevIl;
using Tao.OpenGl;
using Tao.FreeGlut;
using Tao.Platform.Windows;

namespace LabWork_5
{
    public partial class MainForm : Form
    {
        // функция, отрисовывающая усечённый додекаэдр
        private void DrawTruncatedDodecahedron()
        {
            const int decagonalFaceCount = 12; // количество треугольных граней
            const int triangularFaceCount = 20; // количество десятиугольных граней
            const int verticesCount = 60 + 1; // количество вершин фигуры + 1
            float scale = 0.3f; // масштаб отображения фиугры

            int triangularFaceTexture = 4; // номер треугольной грани, на которую наложена текстура
            int decagonalFaceTexture = 0; // номер десятиугольной грани, на которую наложена текстура

            float goldenRatio = (1 + (float)Math.Sqrt(5)) / 2; // значение золотого сечения
            float[][] vertices; // массив координат вершин фигуры
            vertices = new float[verticesCount][]
            {
                new float[] {0, 0, 0}, // фиктивная вершина (для упрощения работы с гранями)

                // первый набор вершин

                new float[] {0, 1 / goldenRatio, (2 + goldenRatio)},
                new float[] {0, 1 / goldenRatio, -(2 + goldenRatio)},
                new float[] {0, -1 / goldenRatio, (2 + goldenRatio)},
                new float[] {0, -1 / goldenRatio, -(2 + goldenRatio)},

                new float[] {(2 + goldenRatio), 0, 1 / goldenRatio},
                new float[] {(2 + goldenRatio), 0, -1 / goldenRatio},
                new float[] {-(2 + goldenRatio), 0, 1 / goldenRatio},
                new float[] {-(2 + goldenRatio), 0, -1 / goldenRatio},

                new float[] {1 / goldenRatio, (2 + goldenRatio), 0},
                new float[] {1 / goldenRatio, -(2 + goldenRatio), 0},
                new float[] {-1 / goldenRatio, (2 + goldenRatio), 0},
                new float[] {-1 / goldenRatio, -(2 + goldenRatio), 0},

                // второй набор вершин

                new float[] {1 / goldenRatio, goldenRatio, 2 * goldenRatio},
                new float[] {1 / goldenRatio, goldenRatio, -2 * goldenRatio},
                new float[] {1 / goldenRatio, -goldenRatio, 2 * goldenRatio},
                new float[] {1 / goldenRatio, -goldenRatio, -2 * goldenRatio},
                new float[] {-1 / goldenRatio, goldenRatio, 2 * goldenRatio},
                new float[] {-1 / goldenRatio, goldenRatio, -2 * goldenRatio},
                new float[] {-1 / goldenRatio, -goldenRatio, 2 * goldenRatio},
                new float[] {-1 / goldenRatio, -goldenRatio, -2 * goldenRatio},

                new float[] {2 * goldenRatio, 1 / goldenRatio, goldenRatio},
                new float[] {2 * goldenRatio, 1 / goldenRatio, -goldenRatio},
                new float[] {2 * goldenRatio, -1 / goldenRatio, goldenRatio},
                new float[] {2 * goldenRatio, -1 / goldenRatio, -goldenRatio},
                new float[] {-2 * goldenRatio, 1 / goldenRatio, goldenRatio},
                new float[] {-2 * goldenRatio, 1 / goldenRatio, -goldenRatio},
                new float[] {-2 * goldenRatio, -1 / goldenRatio, goldenRatio},
                new float[] {-2 * goldenRatio, -1 / goldenRatio, -goldenRatio},

                new float[] {goldenRatio, 2 * goldenRatio, 1 / goldenRatio},
                new float[] {goldenRatio, 2 * goldenRatio, -1 / goldenRatio},
                new float[] {goldenRatio, -2 * goldenRatio, 1 / goldenRatio},
                new float[] {goldenRatio, -2 * goldenRatio, -1 / goldenRatio},
                new float[] {-goldenRatio, 2 * goldenRatio, 1 / goldenRatio},
                new float[] {-goldenRatio, 2 * goldenRatio, -1 / goldenRatio},
                new float[] {-goldenRatio, -2 * goldenRatio, 1 / goldenRatio},
                new float[] {-goldenRatio, -2 * goldenRatio, -1 / goldenRatio},

                // третий набор вершин

                new float[] {goldenRatio, 2, (float)Math.Pow(goldenRatio, 2)},
                new float[] {goldenRatio, 2, -(float)Math.Pow(goldenRatio, 2)},
                new float[] {goldenRatio, -2, (float)Math.Pow(goldenRatio, 2)},
                new float[] {goldenRatio, -2, -(float)Math.Pow(goldenRatio, 2)},
                new float[] {-goldenRatio, 2, (float)Math.Pow(goldenRatio, 2)},
                new float[] {-goldenRatio, 2, -(float)Math.Pow(goldenRatio, 2)},
                new float[] {-goldenRatio, -2, (float)Math.Pow(goldenRatio, 2)},
                new float[] {-goldenRatio, -2, -(float)Math.Pow(goldenRatio, 2)},

                new float[] {(float)Math.Pow(goldenRatio, 2), goldenRatio, 2},
                new float[] {(float)Math.Pow(goldenRatio, 2), goldenRatio, -2},
                new float[] {(float)Math.Pow(goldenRatio, 2), -goldenRatio, 2},
                new float[] {(float)Math.Pow(goldenRatio, 2), -goldenRatio, -2},
                new float[] {-(float)Math.Pow(goldenRatio, 2), goldenRatio, 2},
                new float[] {-(float)Math.Pow(goldenRatio, 2), goldenRatio, -2},
                new float[] {-(float)Math.Pow(goldenRatio, 2), -goldenRatio, 2},
                new float[] {-(float)Math.Pow(goldenRatio, 2), -goldenRatio, -2},

                new float[] {2, (float)Math.Pow(goldenRatio, 2), goldenRatio},
                new float[] {2, (float)Math.Pow(goldenRatio, 2), -goldenRatio},
                new float[] {2, -(float)Math.Pow(goldenRatio, 2), goldenRatio},
                new float[] {2, -(float)Math.Pow(goldenRatio, 2), -goldenRatio},
                new float[] {-2, (float)Math.Pow(goldenRatio, 2), goldenRatio},
                new float[] {-2, (float)Math.Pow(goldenRatio, 2), -goldenRatio},
                new float[] {-2, -(float)Math.Pow(goldenRatio, 2), goldenRatio},
                new float[] {-2, -(float)Math.Pow(goldenRatio, 2), -goldenRatio}
            };

            int[,] triangularFaces; // массив координат вершин, образующих треугольные грани
            triangularFaces = new int[,]
            {
                // в плоскости XoY
                {1, 13, 17},
                {18, 14, 2},
                {15, 3, 19},
                {20, 4, 16},
                {45, 53, 37},
                {38, 54, 46},
                {55, 47, 39},
                {40, 48, 56},    
                {57, 49, 41},
                {42, 50, 58},
                {59, 43, 51},
                {52, 44, 60},
               
                // в плоскости XoZ
                {29, 30, 9},
                {10, 32, 31},
                {11, 34, 33},
                {35, 36, 12},
                
                // в плоскости YoZ
                {22, 6, 24},
                {28, 8, 26},
                {5, 21, 23},
                {27, 25, 7}
            };

            int[,] decagonalFaces; // массив координат вершин, образующих десятиугольные грани
            decagonalFaces = new int[,]
            {
                {1, 17, 41, 49, 25, 27, 51, 43, 19, 3},
                {4, 20, 44, 52, 28, 26, 50, 42, 18, 2},
                {3, 15, 39, 47, 23, 21, 45, 37, 13, 1},
                {2, 14, 38, 46, 22, 24, 48, 40, 16, 4},
                {29, 9, 11, 33, 57, 41, 17, 13, 37, 53},
                {54, 38, 14, 18, 42, 58, 34, 11, 9, 30},
                {15, 19, 43, 59, 35, 12, 10, 31, 55, 39},
                {40, 56, 32, 10, 12, 36, 60, 44, 20, 16},
                {53, 45, 21, 5, 6, 22, 46, 54, 30, 29},
                {6, 5, 23, 47, 55, 31, 32, 56, 48, 24},
                {49, 57, 33, 34, 58, 50, 26, 8, 7, 25},
                {7, 8, 28, 52, 60, 36, 35, 59, 51, 27}
            };

            byte[][] faceColors; // массив, содержащий значения цветов для каждой грани
            faceColors = new byte[triangularFaceCount + decagonalFaceCount][]
            {
                new byte[] {255, 0, 0, 0},
                new byte[] {255, 211, 0, 0},
                new byte[] {191, 167, 48, 0},
                new byte[] {166, 137, 0, 0},
                new byte[] {255, 64, 64, 0},
                new byte[] {255, 116, 0, 0},
                new byte[] {0, 204, 0, 0},
                new byte[] {29, 115, 115, 0},
                new byte[] {0, 99, 99, 0},
                new byte[] {51, 204, 204, 0},
                new byte[] {103, 230, 103, 0},
                new byte[] {255, 231, 115, 0},
                new byte[] {0, 133, 0, 0},
                new byte[] {57, 230, 57, 0},
                new byte[] {92, 204, 204, 0},
                new byte[] {191, 113, 48, 0},
                new byte[] {57, 20, 175, 0},
                new byte[] {65, 44, 132, 0},
                new byte[] {32, 7, 114, 0},
                new byte[] {0, 153, 153, 0},
                new byte[] {38, 153, 38, 0},
                new byte[] {255, 222, 64, 0},
                new byte[] {255, 150, 64, 0},
                new byte[] {106, 72, 215, 0},
                new byte[] {135, 110, 215, 0},
                new byte[] {166, 75, 0, 0},
                new byte[] {255, 178, 115, 0},
                new byte[] {255, 115, 115, 0},
                new byte[] {191, 48, 48, 0},
                new byte[] {72, 3, 111, 0},
                new byte[] {166, 166, 0, 0},
                new byte[] {166, 0, 0, 0}
            };

            // очищаем экран, буфер цвета и глубины
            Gl.glClearColor(255, 255, 255, 0);
            Gl.glClear(Gl.GL_COLOR_BUFFER_BIT | Gl.GL_DEPTH_BUFFER_BIT);

            // включаем отбраковку граней
            Gl.glEnable(Gl.GL_CULL_FACE);
            Gl.glCullFace(Gl.GL_BACK);
            Gl.glFrontFace(Gl.GL_CCW);
            /* If enabled, do depth comparisons and update the depth buffer. Note that even if the depth buffer exists and the depth mask is non-zero, the depth buffer is not updated if the depth test is disabled. */
            Gl.glEnable(Gl.GL_DEPTH_TEST);

            // помещаем объектно-видовую матрицу в стек
            Gl.glPushMatrix();

            // поворачиваем фигуру
            Gl.glRotatef(rotationX, 1, 0, 0);
            Gl.glRotatef(rotationY, 0, 1, 0);
            
            // масштабируем фигуру
            Gl.glScalef(scale, scale, scale);
            Gl.glClearColor(255, 255, 255, 0);

            // отрисовываем треугольные грани
            Gl.glBegin(Gl.GL_TRIANGLES);
            for (int face = 0; face < triangularFaceCount; ++face)
            {
                Gl.glColor4ubv(faceColors[face]); // glColor — set the current color
                if (!((face == triangularFaceTexture) && isTextureLoad))
                    for (int i = 0; i < 3; ++i)
                    {
                        int vertexIndex = triangularFaces[face, i];
                        Gl.glVertex3fv(vertices[vertexIndex]);
                    }
            }
            Gl.glEnd();

            // отрисовываем десятиугольные грани
            for (int face = 0; face < decagonalFaceCount; ++face)
            {
                Gl.glBegin(Gl.GL_POLYGON);
                Gl.glColor4ubv(faceColors[triangularFaceCount + face]); // glColor — set the current color
                if (!((face == decagonalFaceTexture) && isTextureLoad))
                    for (int i = 0; i < 10; ++i)
                    {
                        int vertexIndex = decagonalFaces[face, i];
                        Gl.glVertex3fv(vertices[vertexIndex]);
                    }
                Gl.glEnd();
            }

            // если текстура загружена
            if (isTextureLoad)
            {
                // включаем режим работы с текстурами
                Gl.glEnable(Gl.GL_TEXTURE_2D);
                Gl.glBindTexture(Gl.GL_TEXTURE_2D, glTextureObject);

                // накладываем текстуру на выбранную треугольную грань
                Gl.glBegin(Gl.GL_TRIANGLES);
                Gl.glTexCoord2f(0.5f, 0.0f);
                Gl.glVertex3fv(vertices[triangularFaces[triangularFaceTexture, 0]]);
                Gl.glTexCoord2f(1.0f, 1.0f);
                Gl.glVertex3fv(vertices[triangularFaces[triangularFaceTexture, 1]]);
                Gl.glTexCoord2f(0.0f, 1.0f);
                Gl.glVertex3fv(vertices[triangularFaces[triangularFaceTexture, 2]]);
                Gl.glEnd();

                // накладываем текстуру на выбранную десятиугольную грань
                Gl.glBegin(Gl.GL_POLYGON);
                for (int i = 0; i < 10; i++)
                {
                    Gl.glTexCoord2f((float)Math.Cos((Math.PI / 5) * i), (float)Math.Sin((Math.PI / 5) * i));
                    Gl.glVertex3fv(vertices[decagonalFaces[decagonalFaceTexture, i]]);
                }
                Gl.glEnd();

                // отключаем режим работы с текстурами
                Gl.glDisable(Gl.GL_TEXTURE_2D);
            }

            // получаем из стека матриц объектно-видовую матрицу
            Gl.glPopMatrix();

            // отображаем фигуру
            Gl.glFlush();
            openGlControl.Invalidate();
        }
    }
}
